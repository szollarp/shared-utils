"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getProviderEndpoint = void 0;

var _config = _interopRequireDefault(require("config"));

var _constants = require("./constants");

var getProviderEndpoint = function getProviderEndpoint(endpoint) {
  return "".concat(_config.default.get('general.provider'), "/").concat(_constants.providerEndpoints[endpoint]);
};

exports.getProviderEndpoint = getProviderEndpoint;