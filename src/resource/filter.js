export const filterFields = {
    STRING: [
        'invoiceHead.invoiceData.invoiceNumber',
        'invoiceHead.invoiceData.invoiceCategory',
        'invoiceHead.invoiceData.currencyCode',
        'invoiceHead.customerInfo.customerName',
        'invoiceHead.customerInfo.customerTaxNumber.taxpayerId',
        'invoiceHead.customerInfo.customerTaxNumber.vatCode',
        'invoiceReference.originalInvoiceNumber',
        'source',
        'fileName'
    ],
    NUMBER: [
        'invoiceSummary.summaryNormal.invoiceNetAmount',
        'invoiceSummary.summaryNormal.invoiceVatAmountHUF',
        'invoiceSummary.invoiceGrossAmount',
        'invoiceSummary.summarySimplified.vatContent'
    ],
    DATE: [
        'invoiceHead.invoiceData.invoiceIssueDate'
    ]
}

export const filterComparers = {
    STRING: [
        'IS_PROVIDED',
        'IS_NOT_PROVIDED',
        'STARTS_WITH',
        'NOT_STARTS_WITH',
        'STARTS_WITH_LETTER',
        'NOT_STARTS_WITH_LETTER',
        'STARTS_WITH_NUMBER',
        'NOT_STARTS_WITH_NUMBER',
        'ENDS_WITH',
        'NOT_ENDS_WITH',
        'CONTAINS',
        'NOT_CONTAINS',
        'EQUALS',
        'NOT_EQUALS'
    ],
    NUMBER: [
        'EQUALS',
        'NOT_EQUALS',
        'IS_NOT_PROVIDED',
        'LESS_THAN',
        'LESS_THAN_EQUAL',
        'GREATER_THAN',
        'GREATER_THAN_EQUAL'
    ],
    DATE: [
        'DATE_BEFORE',
        'DATE_AFTER',
        'IS_NOT_PROVIDED'
    ]
}

export const comparersWithNoValue = [
    'IS_PROVIDED','IS_NOT_PROVIDED','STARTS_WITH_LETTER','NOT_STARTS_WITH_LETTER','STARTS_WITH_NUMBER',
    'NOT_STARTS_WITH_NUMBER',
]