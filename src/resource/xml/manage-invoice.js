
export default `
  <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
  <ManageInvoiceRequest xmlns="http://schemas.nav.gov.hu/OSA/1.0/api">
    <header>
      <requestId></requestId>
      <timestamp></timestamp>
      <requestVersion>1.0</requestVersion>
      <headerVersion>1.0</headerVersion>
    </header>
    <user>
      <login></login>
      <passwordHash></passwordHash>
      <taxNumber></taxNumber>
      <requestSignature></requestSignature>
    </user>
    <software></software>
    <exchangeToken></exchangeToken>
    <invoiceOperations>
      <technicalAnnulment>false</technicalAnnulment>
      <compressedContent>false</compressedContent>
      <invoiceOperation>
        <index>1</index>
        <operation>CREATE</operation>
        <invoice></invoice>
      </invoiceOperation>
    </invoiceOperations>
  </ManageInvoiceRequest>
`
