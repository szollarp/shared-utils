export default `
  <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
  <QueryInvoiceStatusRequest xmlns="http://schemas.nav.gov.hu/OSA/1.0/api">
    <header>
      <requestId></requestId>
      <timestamp></timestamp>
      <requestVersion>1.0</requestVersion>
      <headerVersion>1.0</headerVersion>
    </header>
    <user>
      <login></login>
      <passwordHash></passwordHash>
      <taxNumber></taxNumber>
      <requestSignature></requestSignature>
    </user>
    <software></software>
    <transactionId></transactionId>
    <returnOriginalRequest>false</returnOriginalRequest>
  </QueryInvoiceStatusRequest>
`
