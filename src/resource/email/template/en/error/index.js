import details from './details'

export default (notificationObject) => {
    return `<p>Dear Partner,</p>
    <p>The following error occured during an invoice transaction:</p>
    <p>Error code: ${notificationObject.errorCode}</p>
    <p>Error message: ${notificationObject.message}</p>
    <p>Error source: ${notificationObject.source}</p>
    ${details(notificationObject)}
    `
}