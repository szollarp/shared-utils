export default (notificationObject) => {
    if (notificationObject.detailedErrorCode && notificationObject.detailedMessage) {
        return `<p>Details:
        <br>Error code: ${notificationObject.detailedErrorCode}
        <br>Error message: ${notificationObject.detailedMessage}
        </p>`
    } else {
        return ''
    }
}