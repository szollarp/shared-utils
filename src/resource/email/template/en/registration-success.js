export default () => {
    return `<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
			<head>
			  <title>ConnecTAX</title>
			  <meta http-equiv="Content-type" content="text/html; charset=utf-8">
			  <meta name="description" content="">
			  <meta name="distribution" content="local">
			  <meta name="keywords" content="">
			  <meta name="copyright" content="All rights reserved">
			  <meta name="robots" content="index, follow">
			  <!-- NAME: 1:3:2 COLUMN - BANDED --><!--[if gte mso 15]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
			  <meta charset="UTF-8">
			  <meta http-equiv="X-UA-Compatible" content="IE=edge">
			  <meta name="viewport" content="width=device-width, initial-scale=1">
			  <style type="text/css">html,body {
			  	color: #57585b;
				font-family: Helvetica, Arial, sans-serif;
				font-size: 16px;
				}p {
				margin: 10px 0;
				padding: 0;
				}table {
				border-collapse: collapse;
				}h1,h2,h3,h4,h5,h6 {
				display: block;
				margin: 0;
				padding: 0;
				color: #2d2d2d;
				}img,a img {
				border: 0;
				height: auto;
				outline: none;
				text-decoration: none;
				}body,
				#bodyTable,
				#bodyCell {
				height: 100%;
				margin: 0;
				padding: 0;
				width: 100%;
				}#outlook a {
				padding: 0;
				}img {
				-ms-interpolation-mode: bicubic;
				}table {
				mso-table-lspace: 0pt;
				mso-table-rspace: 0pt;
				}.ReadMsgBody {
				width: 100%;
				}.ExternalClass {
				width: 100%;
				}p,a,li,td,blockquote {
				mso-line-height-rule: exactly;
				}a[href^=tel],
				a[href^=sms] {
				color: inherit;
				cursor: default;
				text-decoration: none;
				}p,a,li,td,body,table,blockquote {
				-ms-text-size-adjust: 100%;
				-webkit-text-size-adjust: 100%;
				}.ExternalClass,
				.ExternalClass p,
				.ExternalClass td,
				.ExternalClass div,
				.ExternalClass span,
				.ExternalClass font {
				line-height: 100%;
				}a[x-apple-data-detectors] {
				color: inherit !important;
				text-decoration: none !important;
				font-size: inherit !important;
				font-family: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
				}.templateContainer {
				max-width: 600px !important;
				}a.mcnButton {
				display: block;
				text-transform: uppercase;
				}.mcnImage {
				vertical-align: bottom;
				}.mcnTextContent {
				word-break: break-word;
				}.mcnTextContent img {
				height: auto !important;
				}.mcnDividerBlock {
				table-layout: fixed !important;
				}body,#bodyTable {
				background-color: #ffffff;
				}#bodyCell {
				border-top: 0;
				}h1 {
				color: #ffffff;
				font-family: Helvetica, Arial, sans-serif;
				font-size: 32px;
				font-style: normal;
				font-weight: bold;
				line-height: 125%;
				letter-spacing: normal;
				text-align: left;
				}h2 {
				color: #2d2d2d;
				font-family: Helvetica, Arial, sans-serif;
				font-size: 22px;
				font-style: normal;
				font-weight: normal;
				line-height: 125%;
				letter-spacing: normal;
				text-align: left;
				}h3 {
				color: #2d2d2d;
				font-family: Helvetica, Arial, sans-serif;
				font-size: 21px;
				font-style: normal;
				font-weight: normal;
				line-height: 125%;
				letter-spacing: normal;
				text-align: left;
				}h4 {
				color: #2d2d2d;
				font-family: Helvetica, Arial, sans-serif;
				font-size: 16px;
				font-style: normal;
				font-weight: bold;
				line-height: 125%;
				letter-spacing: normal;
				text-align: left;
				}#templatePreheader {
				background-color: #ffffff;
				border-top: 0;
				border-bottom: 0;
				padding-top: 0px;
				padding-bottom: 0px;
				}#templatePreheader .mcnTextContent,
				#templatePreheader .mcnTextContent p {
				color: #57585b;
				font-family: Helvetica, Arial, sans-serif;
				font-size: 16px;
				line-height: 150%;
				text-align: left;
				}#templatePreheader .mcnTextContent a,
				#templatePreheader .mcnTextContent p a {
				color: #009de0;
				font-weight: normal;
				text-decoration: underline;
				}#templateHeader {
				background-color: #009de0;
				border-top: 0;
				border-bottom: 0;
				padding-top: 10px;
				padding-bottom: 0;
				}#templateHeader .mcnTextContent,
				#templateHeader .mcnTextContent p {
				margin-top: 0;
				color: #57585b;
				font-family: Helvetica, Arial, sans-serif;
				font-size: 10px;
				line-height: 150%;
				text-align: left;
				}#templateHeader .mcnTextContent a,
				#templateHeader .mcnTextContent p a {
				color: #009de0;
				font-weight: normal;
				text-decoration: underline;
				}#templateHeader .mcnLogo a {
				display: block;
				width: 122px;
				height: 51px;
				}#templateHeader .mcnLogo img {
				display: block;
				width: 122px;
				max-width: 122px;
				height: 51px;
				max-height: 51px;
				}#baseTextBlock {
				padding-top: 20px;
				padding-bottom: 20px;
				background-color: #ffffff;
				border-top: 0;
				border-bottom: 0;
				}#baseTextBlock .mcnTextBlock p {
				line-height: 150%;
				font-family: Helvetica, Arial, sans-serif;
				font-size: 16px;
				text-align: left;
				}#baseTextBlock .mcnTextBlock a {
				color: #009de0;
				text-decoration: none;
				}#templatePreFooter {
				background-color: #fff;
				border-top: 0;
				border-bottom: 0;
				padding-top: 20px;
				padding-bottom: 20px;
				}#templatePreFooterLegal {
				background-color: #f2f2f2;
				border-top: 0;
				border-bottom: 0;
				padding-top: 0;
				padding-bottom: 20px;
				}#templatePreFooter .mcnTextContent,
				#templatePreFooter .mcnTextContent p {
				color: #999999;
				font-family: Helvetica, Arial, sans-serif;
				font-size: 25px;
				line-height: 150%;
				text-align: left;
				}#templatePreFooterLegal .mcnTextContent,
				#templatePreFooterLegal .mcnTextContent p {
				color: #999999;
				font-family: Helvetica, Arial, sans-serif;
				font-size: 12px;
				text-align: left;
				}#templateFooter {
				background-color: #a0b2c6;
				border-top: 0;
				border-bottom: 0;
				padding-top: 20px;
				padding-bottom: 20px;
				}#templateFooter .mcnTextContent,
				#templateFooter .mcnTextContent p {
				color: #57585b;
				font-family: Helvetica, Arial, sans-serif;
				font-size: 12px;
				line-height: 150%;
				text-align: left;
				}#templateFooter .mcnTextContent a,
				#templateFooter .mcnTextContent p a {
				color: #009de0;
				font-weight: normal;
				text-decoration: underline;
				}@media only screen and (min-width:768px){.templateContainer {
				width: 600px !important;
				}}@media only screen and (max-width: 480px){body,table,td,p,a,li,blockquote {
				-webkit-text-size-adjust: none !important;
				}}@media only screen and (max-width: 480px){body {
				width: 100% !important;
				min-width: 100% !important;
				}}@media only screen and (max-width: 480px){ #bodyCell {
				padding-top: 10px !important;
				}}@media only screen and (max-width: 480px){.columnWrapper {
				max-width: 100% !important;
				width: 100% !important;
				}}@media only screen and (max-width: 480px){.mcnImage {
				width: 100% !important;
				}}@media only screen and (max-width: 480px){.mcnCartContainer,
				.mcnCaptionTopContent,
				.mcnRecContentContainer,
				.mcnCaptionBottomContent,
				.mcnTextContentContainer,
				.mcnBoxedTextContentContainer,
				.mcnImageGroupContentContainer,
				.mcnCaptionLeftTextContentContainer,
				.mcnCaptionRightTextContentContainer,
				.mcnCaptionLeftImageContentContainer,
				.mcnCaptionRightImageContentContainer,
				.mcnImageCardLeftTextContentContainer,
				.mcnImageCardRightTextContentContainer {
				max-width: 100% !important;
				width: 100% !important;
				}}@media only screen and (max-width: 480px){.mcnBoxedTextContentContainer {
				min-width: 100% !important;
				}}@media only screen and (max-width: 480px){.mcnImageGroupContent {
				padding: 20px 10px !important;
				}}@media only screen and (max-width: 480px){.mcnCaptionLeftContentOuter .mcnTextContent,
				.mcnCaptionRightContentOuter .mcnTextContent {
				padding-top: 20px !important;
				}}@media only screen and (max-width: 480px){.mcnImageCardTopImageContent,
				.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
				padding-top: 20px !important;
				}}@media only screen and (max-width: 480px){.mcnImageCardBottomImageContent {
				padding-bottom: 20px !important;
				}}@media only screen and (max-width: 480px){.mcnImageGroupBlockInner {
				padding-top: 0 !important;
				padding-bottom: 0 !important;
				}}@media only screen and (max-width: 480px){.mcnImageGroupBlockOuter {
				padding-top: 20px !important;
				padding-bottom: 20px !important;
				}}@media only screen and (max-width: 480px){.mcnTextContent,
				.mcnBoxedTextContentColumn {
				padding-right: 10px !important;
				padding-left: 10px !important;
				}}@media only screen and (max-width: 480px){.mcnImageCardLeftImageContent,
				.mcnImageCardRightImageContent {
				padding-right: 10px !important;
				padding-bottom: 0 !important;
				padding-left: 10px !important;
				}}@media only screen and (max-width: 480px){.mcpreview-image-uploader {
				display: none !important;
				width: 100% !important;
				}}@media only screen and (max-width: 480px){.mcnBoxedTextContentContainer .mcnTextContent,
				.mcnBoxedTextContentContainer .mcnTextContent p {
				font-size: 16px !important;
				line-height: 150% !important;
				}}@media only screen and (max-width: 480px){ #templatePreheader {
				display: block !important;
				}}@media only screen and (max-width: 480px){ #templatePreFooter .mcnTextContent,
				#templatePreFooter .mcnTextContent p {
				font-size: 15px;
				}}
			  </style>
			</head>
			<body>
			  <center>
				<table id="bodyTable" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">
					<tbody>
					  <tr>
						<td id="bodyCell" valign="top" align="center">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
							  <tbody>
								<tr>
									<td id="templateHeader" valign="top" align="center">
									  <!--[if gte mso 9]><table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;"><tr><td align="center" valign="top" width="600" style="width:600px;"><![endif]-->
									  <table class="templateContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
										<tbody>
											<tr>
											  <td class="bodyContainer" valign="top">
												<table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
													<tbody class="mcnTextBlockOuter">
													  <tr>
															<td class="mcnTextContent" style="padding-top:20px; padding-bottom: 20px; padding-right:10px; padding-left:10px;" valign="middle">
																<h1>ConnecTAX</h1>
															</td>
													  </tr>
													</tbody>
												</table>
											  </td>
											</tr>
										</tbody>
									  </table>
									  <!--[if gte mso 9]></td></tr></table><![endif]-->
									</td>
								</tr>
								<tr>
									<td id="bodyCell" valign="top" align="center">
									  <table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
											  <td id="baseTextBlock" valign="top" align="center">
											  <!--[if gte mso 9]><table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;"><tr><td align="center" valign="top" width="600" style="width:600px;"><![endif]-->
												<table class="templateContainer" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
													<tbody>
													  <tr>
														<td class="bodyContainer" valign="top">
															<table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
															  <tbody class="mcnTextBlockOuter">
																<tr>
																	<td class="mcnTextBlockInner" style="color: #57585b; font-family: Helvetica, Arial, sans-serif; font-size: 16px; padding: 14px 10px;" valign="top">

																		<!-- CONTENT -->

																		<h2 style="text-transform: uppercase;"><b>Dear Partner</b></h2>
																		<br /><br />
                                                                        <p style="box-sizing: border-box; margin: 0px 0px 10px; color: #57585b; font-family: Helvetica, Arial, sans-serif; font-size: 16px;">Thank you for your registration to RSM ConnecTAX</p>
                                                                        <p style="box-sizing: border-box; margin: 0px 0px 10px; color: #57585b; font-family: Helvetica, Arial, sans-serif; font-size: 16px;">You can sign in with the link below:</p>
																		<p style="box-sizing: border-box; margin: 0px 0px 40px; color: #57585b; font-family: Helvetica, Arial, sans-serif; font-size: 16px;"><a href="http://connectax-test.rsm.hu:9005/login">Sign in to RSM ConnecTAX</a></p>

																		<!-- END CONTENT -->

																	</td>
																</tr>
															  </tbody>
															</table>
														</td>
													  </tr>
													</tbody>
												</table>
											  </td>
											</tr>
										</tbody>
									  </table>
									</td>
								</tr>

								<tr>
								    <td id="templatePreFooterLegal" valign="top" align="center">
									  <!--[if gte mso 9]><table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;"><tr><td align="center" valign="top" width="600" style="width:600px;"><![endif]-->
									  <table class="templateContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
										<tbody>
										    <tr>
											  <td class="footerContainer" valign="top">
												<table width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
												    <tbody>
													  <tr>
														<td valign="top" align="left">
														    <table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
															  <tbody class="mcnTextBlockOuter">
																<tr>
																    <td class="mcnTextBlockInner" style="padding-top:0px;" valign="top">
																    <!--[if mso]><table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;"><tr><![endif]--><!--[if mso]><td valign="top" width="600" style="width:600px;"><![endif]-->
																	  <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
																		<tbody>
																		    <tr>
																			  <td class="mcnTextContent" style="padding-top:30px; padding-right:10px; padding-bottom:10px; padding-left:10px;" valign="top">
																				<p style="margin: 0px; line-height: 1.25; color: #999999; font-family: Helvetica, Arial, sans-serif; font-size: 12px;">The information contained in this e-mail message is intended only for the person or entity to which it is addressed and may contain confidential information.

																				Any review, retransmission, dissemination or other use of, or taking of any action in reliance upon, this information by persons or entities other than

																				the intended recipient is prohibited. If you received this e-mail message in error, please immediately contact the sender and delete the received e-mail message

																				from any computer.</p>
																			  </td>
																		    </tr>
																		</tbody>
																	  </table>
																	  <!--[if mso]></td><![endif]--><!--[if mso]></tr></table><![endif]-->
																    </td>
																</tr>
															  </tbody>
														    </table>
														</td>
													  </tr>
												    </tbody>
												</table>
											  </td>
										    </tr>
										</tbody>
									  </table>
									  <!--[if gte mso 9]></td></tr></table><![endif]-->
								    </td>
								</tr>

								<tr>
									<td id="templateFooter" valign="top" align="center">
									  <!--[if gte mso 9]><table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;"><tr><td align="center" valign="top" width="600" style="width:600px;"><![endif]-->
									  <table class="templateContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
										<tbody>
											<tr>
											  <td class="footerContainer" valign="top">
												<table width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
													<tbody>
													  <tr>
														<td valign="top" align="left">
															<table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
															  <tbody class="mcnTextBlockOuter">
																<tr>
																	<td class="mcnTextBlockInner" style="padding-top:0px;" valign="top">
																	  <!--[if mso]><table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;"><tr><![endif]--><!--[if mso]><td valign="top" width="600" style="width:600px;"><![endif]-->
																	  <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
																		<tbody>
																			<tr>
																			  <td class="mcnTextContent" style="padding-top:0; padding-right:10px; padding-bottom:0px; padding-left:10px; color:#ffffff;" valign="top">
																				&copy; 2018 ConnecTAX
																			  </td>
																			</tr>
																		</tbody>
																	  </table>
																	<!--[if mso]></td><![endif]--><!--[if mso]></tr></table><![endif]-->
																	</td>
																</tr>
															  </tbody>
															</table>
														</td>
													  </tr>
													</tbody>
												</table>
											  </td>
											</tr>
										</tbody>
									  </table>
									  <!--[if gte mso 9]></td></tr></table><![endif]-->
									</td>
								</tr>
							  </tbody>
							</table>
						</td>
					  </tr>
					</tbody>
				</table>
			  </center>
			</body>
		</html>`
}
