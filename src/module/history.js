import CollectionFactory from '../store/factories/factory';
import CustomerFactory from '../database/factory/CustomerFactory';
import HistoryFactory from '../database/factory/HistoryFactory';
import logger from './logger';

export default async (collection, object, code, attachment, context, user = null) => {
    let objectWithUpdatedHistory = object;

    try {
        const collectionFactory = new CollectionFactory();
        collectionFactory.collectionName = collection;
        collectionFactory.historyEnabled = false;

        const customerFactory = new CustomerFactory();
        const historyFactory = new HistoryFactory();
        let customer = null,
            userId = null;

        if (context) {
            customer = await customerFactory.getOne({
                context
            });
            if (!customer) {
                throw new Error('Invalid customer');
            }
        }

        if (user) {
            userId = user;
        } else if (customer) {
            userId = customer.createdBy;
        }

        const addingHistory = {
            code,
            attachment,
            createdAt: Date.now(),
            createdBy: userId
        };

        const history = await historyFactory.create({ ...addingHistory
        }, context);

        if (objectWithUpdatedHistory._id) {
            const objectToAddHistoryId = await collectionFactory.getOne({
                _id: object._id.toString()
            }, context);
            const updatedObjectHistory = objectToAddHistoryId.history || [];
            updatedObjectHistory.push(history._id.toString());
            objectWithUpdatedHistory = await collectionFactory.update(object._id.toString(), {
                history: updatedObjectHistory
            }, context);
        } else {
            if (!objectWithUpdatedHistory.history) {
                objectWithUpdatedHistory.history = [];
            }
            objectWithUpdatedHistory.history.push(history._id.toString());
        }

    } catch (err) {
        logger.error(`ADDING HISTORY for object by id ${object._id} by code ${code} with context ${context} and user ${user} *** ${err.message}`);
    }

    return objectWithUpdatedHistory;
};