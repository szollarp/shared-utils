import xml2js from 'xml2js'
import moment from 'moment'
import sha512 from 'js-sha512'
import providerXMLTemplateContent from '../../resource/xml/token-exchange';

export default (customerConfig, invoicingSoftwareObject) => {
	let generatedXML = null

	try {
		let providerXMLTemplateObject = null

		xml2js.parseString(providerXMLTemplateContent, (err, data) => {
			if (err) {
				throw new Error(err)
			} else {
				providerXMLTemplateObject = data
			}
		})

		const requestId = 'RID' + new Date().getTime()
		providerXMLTemplateObject.TokenExchangeRequest.header[0].requestId = requestId

		const timeStringNow = moment.utc().format('YYYY-MM-DDTHH:mm:ss.SSS')
		const timeStringNowZoned = timeStringNow + 'Z'
		const timeStringNowFormatted = moment(timeStringNow).format('YYYYMMDDHHmmss')
		providerXMLTemplateObject.TokenExchangeRequest.header[0].timestamp = timeStringNowZoned

		const login = customerConfig.login
		const passwordHash = sha512(customerConfig.password).toUpperCase()
		providerXMLTemplateObject.TokenExchangeRequest.user[0].login = login
		providerXMLTemplateObject.TokenExchangeRequest.user[0].passwordHash = passwordHash
		providerXMLTemplateObject.TokenExchangeRequest.user[0].taxNumber = customerConfig.taxNumber

		const requestSignature = sha512(requestId + timeStringNowFormatted + customerConfig.signKey).toUpperCase()
		providerXMLTemplateObject.TokenExchangeRequest.user[0].requestSignature = requestSignature

		providerXMLTemplateObject.TokenExchangeRequest.software.length = 0
		if (invoicingSoftwareObject) {
			providerXMLTemplateObject.TokenExchangeRequest.software.push({})
			if (invoicingSoftwareObject.softwareId) {
				providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareId = invoicingSoftwareObject.softwareId
			}
			if (invoicingSoftwareObject.softwareName) {
				providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareName = invoicingSoftwareObject.softwareName
			}
			if (invoicingSoftwareObject.softwareOperation) {
				providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareOperation = invoicingSoftwareObject.softwareOperation
			}
			if (invoicingSoftwareObject.softwareMainVersion) {
				providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareMainVersion = invoicingSoftwareObject.softwareMainVersion
			}
			if (invoicingSoftwareObject.softwareDevName) {
				providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareDevName = invoicingSoftwareObject.softwareDevName
			}
			if (invoicingSoftwareObject.softwareDevContact) {
				providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareDevContact = invoicingSoftwareObject.softwareDevContact
			}
			if (invoicingSoftwareObject.softwareDevCountryCode) {
				providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareDevCountryCode = invoicingSoftwareObject.softwareDevCountryCode
			}
			if (invoicingSoftwareObject.softwareDevTaxNumber) {
				providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareDevTaxNumber = invoicingSoftwareObject.softwareDevTaxNumber
			}
		}

		const xmlBuilder = new xml2js.Builder();

		generatedXML = xmlBuilder.buildObject(providerXMLTemplateObject)

	} catch (err) {
		throw new Error(err)
	}

	return generatedXML
}