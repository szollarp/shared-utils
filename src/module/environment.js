const { lstatSync, readdirSync, existsSync } = require('fs')
const { join, sep } = require('path')

const isDirectory = file => {
	if ( !existsSync(file) ) {
		return false
	}

	return lstatSync(file).isDirectory()
}
const getDirectories = file =>
    readdirSync(file).map(name => join(file, name)).filter(isDirectory)

const getFfpUsernameFromFileName = fileName => {
	const schemas = {
		small: fileName.match(/(\/)([a-zA-Z0-9]{25})(\/)/i),
		medium: fileName.match(/(\/)([a-zA-Z0-9]{31})(\/)/i),
	}

	if (schemas.small && schemas.small[2] || (schemas.medium && schemas.medium[2])) {
		return schemas.small ? schemas.small[2] : schemas.medium[2]
	}

	return false
}

export const hasUploadsFolder = (directory, file) => {
    const userDirectories = getDirectories(directory)
    const ftpUsername = getFfpUsernameFromFileName(file)

    if (!ftpUsername) {
        return false
    }

    const userDirectory = userDirectories.filter(dirPath => {
        const username = dirPath.split(sep).pop()
        return username === ftpUsername
    }).pop()

    const uploadDirectories = getDirectories(userDirectory)
    const uploadsDirectory = uploadDirectories.filter(uploadDir => {
        const dirname = uploadDir.split(sep).pop()
        return dirname === 'uploads'
    })

    return (uploadsDirectory.length > 0)
}
