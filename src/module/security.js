import { Buffer } from 'buffer';

export const stringToBase64 = string => (
    new Buffer(string, 'utf8').toString('base64')
);

export const base64ToString = base64 => (
    new Buffer(base64, 'base64').toString('utf8')
);