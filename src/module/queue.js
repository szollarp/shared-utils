import RedisSMQ from 'rsmq';
import RSMQWorker from 'rsmq-worker';

import logger from './logger';
import config from 'config';

const {
    host,
    port,
    ns
} = config.get('queue.general');

const rsmq = new RedisSMQ({
    host,
    port,
    ns
});

const createQueue = (qname) => {
    rsmq.createQueue({
        qname
    }, function (err, resp) {
        if (resp === 1) {
            logger.info(`REDIS QUEUE *** Queue ${qname} created`)
        }
    });
};

export const sendMessageToQueue = async (qname, message) => {
    await createQueue(qname)
    rsmq.sendMessage({
        qname,
        message
    }, function (err, resp) {
        if (err) {
            logger.error(`REDIS QUEUE *** ${qname} *** ${err.message}`)
        }
        if (resp) {
            logger.info(`REDIS QUEUE *** Message added to ${qname} queue: ${message}`)
        }
    });
};

export const watchQueueMessages = (qname, callback) => {
    const worker = new RSMQWorker(qname, {
        workerConfig: config.get('queue.worker'),
        rsmq
    });

    worker.on("message", async (msg, next, id) => {
        callback(msg, id).then(res => {
            if (res.success || res.error) {
                worker.del(id, () => {
                    logger.info(`REDIS QUEUE *** Message delete from ${qname}: ${id}`);
                });
            }
        }).catch(e => {
            logger.error(`ERROR ON REDIS QUEUE *** ${qname}`);
            logger.error(e.message);
        });
    });

    worker.on('error', (err, msg) =>
        logger.error(`REDIS QUEUE *** Queue ${qname} error on message: ${msg.id}, ${err}`));

    worker.on('exceeded', msg =>
        logger.warning(`REDIS QUEUE *** Queue ${qname} exceeded on message: ${msg.id}`));

    return worker;
}

export const deleteMessageFromQueue = (qname, id) => {
    rsmq.deleteMessage({
        qname,
        id
    }, function (err, resp) {
        if (err) {
            logger.error(`REDIS QUEUE *** ${qname} *** ${err.message}`);
        }
        if (resp) {
            logger.info(`REDIS QUEUE *** Message removed from ${qname} by id: ${id}`);
        }
    });
}