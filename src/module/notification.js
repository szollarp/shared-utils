import CircularJSON from 'circular-json';
import config from 'config';
import { notificationCodesToPassQueueById } from '../constants';
import { sendMessageToQueue } from './queue';

const queueName = get.config('service.notifier.queueConfig.queueName');
export const handleNotification = async (type, payload, customerHash = null) => {
    let sendIdOnly = false;
    for (let entityType in notificationCodesToPassQueueById) {
        if (notificationCodesToPassQueueById[entityType].includes(type)) {
            sendIdOnly = true
            break
        };
    };

    const notification = {
        type,
        payload: sendIdOnly ? { _id: payload._id } : payload,
        customerHash
    };

    const message = CircularJSON.stringify(notification);
    await sendMessageToQueue(queueName, message);
};

export const handleNotificationWithEmails = async (type, payload, emails = []) => {
    let sendIdOnly = false;
    for (let entityType in notificationCodesToPassQueueById) {
        if (notificationCodesToPassQueueById[entityType].includes(type)) {
            sendIdOnly = true;
            break;
        };
    };

    payload = sendIdOnly ? { _id: payload._id } : payload;
    const notificationObject = {
        type,
        payload: {
			...payload,
			uiHost: config.get('email.uiHost')
		},
        emails
    };

    const message = CircularJSON.stringify(notificationObject);
    await sendMessageToQueue(queueName, message);
}
