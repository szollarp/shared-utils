import camelcase from 'camelcase';
import CustomerFactory from '../database/factory/CustomerFactory';

export const createMailContentByNotification = async notificationObject => {
    const lang = 'en'
	const notificationType = notificationObject.type
    let templateModuleName = camelcase(notificationType)
    templateModuleName = templateModuleName.replace('irina','')
    templateModuleName = templateModuleName.charAt(0).toLowerCase() + templateModuleName.slice(1)
    let templateModule = null
    let mailContent = null
    let customerName = ''

    try {
        const templateModules = require(`../../resources/email/template/${lang}`)
        templateModule = templateModules[templateModuleName]
        if (!templateModule) {
            throw new Error(`no template found with name '${templateModuleName}'`)
        }

        if (notificationObject.customerHash) {
			const customerFactory = new CustomerFactory()
            const customer = await customerFactory.getOne({ context: notificationObject.customerHash })
            customerName = (customer && customer.name) ? customer.name : ''
        }

        if (!notificationObject.payload.customerName && notificationObject.type !== 'CUSTOM_NOTIFICATIONS') {
            notificationObject.payload.customerName = customerName
        }

        mailContent = templateModule(notificationObject.payload)
    } catch(err) {
        throw new Error(err)
    }

    return mailContent
}
