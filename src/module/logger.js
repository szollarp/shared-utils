import winston from 'winston';

const messageFormat = winston.format.printf(info => {
	return `${info.timestamp} [${info.level}] ${info.message}`
});

const logger = winston.createLogger({
	levels: winston.config.syslog.levels,
	format: winston.format.combine(
		winston.format.timestamp(),
		messageFormat
	),
	transports: [
		new winston.transports.Console({
			format: winston.format.combine(
				winston.format.colorize(),
				winston.format.timestamp(),
				messageFormat
			)
		})
	]
});

export default logger;
