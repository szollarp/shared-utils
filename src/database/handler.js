import {
    MongoClient
} from 'mongodb';
import config from 'config';

export const handleDbQuery = async (callback, database = null) => {
    const dbName = !database ? config.get('database.database') : database;

    try {
        const client = await MongoClient.connect(config.get('database.host'));
        const db = client.db(dbName);

        const response = await callback(db);
        if (client) {
            client.close();
        }

        return response;

    } catch (err) {
        console.error(err.toString());
        throw new Error(err);
    }
};