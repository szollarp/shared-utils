import CollectionFacotry from './Factory'
import { handleDbQuery } from '../handler'

class InvoiceReportFactory extends CollectionFacotry {
	constructor() {
		super()
		this.collectionName = 'invoicereports'

		this.validationRules = {
			create: [],
			update: []
		}
	}

	async create(args, context) {
		if(!context) {
			throw new Error('Invalid data')
		}

		try {
			const invoiceResponseData = {
				createdAt: Date.now(),
				createdBy: null,
				softwareId: null,
				...args,
				receives: [],
				sends: [],
				errorResults: [],
				warningResults: []
			}

			return await handleDbQuery( async db => {
				const response = await db.collection(this.collectionName).insertOne(invoiceResponseData)
				return response.ops.pop()
			}, context )
		} catch (err) {
			throw new Error(err)
		}
	}
}

export default InvoiceReportFactory
