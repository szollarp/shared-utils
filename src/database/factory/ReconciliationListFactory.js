import CollectionFacotry from './Factory'

class ReconciliationListFactory extends CollectionFacotry {
	constructor() {
		super()
		this.collectionName = 'reconciliationlists'

		this.validationRules = {
			create: [],
			update: []
		}
	}
}

export default ReconciliationListFactory
