import { presence } from 'property-validator'

import CollectionFactory from './Factory'
import UserFactory from './UserFactory'

class FilterFactory extends CollectionFactory {
	constructor() {
		super()
        this.collectionName = 'filters'
        this.userFactory = new UserFactory()

		this.validationRules = {
			create: [
				presence('name'),
                presence('enabled'),
                presence('conditions')
			],
			update: [
                presence('name'),
                presence('enabled'),
                presence('conditions')
			]
		}
	}

	async create(args, user, context) {
		if(!context) {
			throw new Error('Invalid data')
		}

        user = await this.userFactory.getOne({ _id: user._id })
        if (!user) {
            throw new Error('Invalid user')
        }

		try {
			const filter = {
                ...args,
                createdBy: user._id
			}

			return await super.create({ ...filter }, context)
		} catch (err) {
			throw new Error(err)
		}
	}

	async update(id, args, user, context) {
		const { name, enabled, conditions } = args
        if(!context) {
			throw new Error('Invalid data')
        }

        user = await this.userFactory.getOne({ _id: user._id })
        if (!user) {
            throw new Error('Invalid user')
        }

		try {
			const filter = {
				enabled,
				updatedBy: user._id
			}
			if (name && conditions) {
				filter.name = name
				filter.conditions = conditions
			}

			await super.update(id, { ...filter }, context)
			return this.get({}, context)
		} catch (err) {
			throw new Error(err)
		}
	}

	async remove(id, user, context) {
		if(!context) {
			throw new Error('Invalid data')
		}

		user = await this.userFactory.getOne({ _id: user._id })
        if (!user) {
            throw new Error('Invalid user')
		}

		try {
            await super.remove(id, context)
			return this.get({}, context)
		} catch (err) {
			throw new Error(err)
		}
	}
}

export default FilterFactory
