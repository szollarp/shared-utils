import CollectionFacotry from './Factory'

class KeyFactory extends CollectionFacotry {
	constructor() {
		super()
		this.collectionName = 'keys'

		this.validationRules = {
			create: [],
			update: []
		}
	}
}

export default KeyFactory
