import bcrypt from 'bcrypt'
import owasp from 'owasp-password-strength-test'
import config from 'config'
import { validate, presence, email } from 'property-validator'
import { handleDbQuery } from '../handler'
import CollectionFacotry from './Factory'
import InvitationFactory from './InvitationFactory'

const USER_TYPE_ACTIVE = 'ACTIVE'

class UserFactory extends CollectionFacotry {
	constructor() {
		super()
		this.collectionName = 'users'

		this.validationRules = {
			create: [
				presence('firstName'),
				presence('lastName'),
				presence('password'),
				email('email')
			],
			update: [
				presence('firstName'),
				presence('lastName'),
				presence('password'),
				email('email')
			]
		}

		this.invitationFactory = new InvitationFactory()
	}

	validatePassword(password, passwordVerify) {
		if (password !== passwordVerify) return false

		const pswTest = owasp.test(password)
		if(pswTest.errors.length > 0) return false

		return true
	}

	async create(args) {
		if(!this.validatePassword(args.password, args.passwordVerify)) {
			throw new Error('Missing or invalid registration data')
		}

		let invitation = null
		let context = null
		if (args.token && args.context) {
			invitation = await this.invitationFactory.getOne({ token: args.token }, args.context)
			context = args.context
		}

		delete args.passwordVerify
		delete args.token
		delete args.context

		const { asInvite, asRegistration } = config.get('defaults.licences')
		let userData = {
			...args,
			password: await bcrypt.hash(args.password, 12),
			status: USER_TYPE_ACTIVE,
			createdAt: Date.now(),
			config: {
				softwarePerCustomer: asInvite || asRegistration
			}
		}

		try {
			validate(userData, this.validationRules.create)
			return await handleDbQuery(async db => {
				const userExists = await db.collection(this.collectionName).findOne({ email: userData.email })
				if(!userExists) {
					const newUser = await super.create({ ...userData })
					if (invitation) {
						await this.pushChildren(newUser._id.toString(), {
							permissions: { context, ...invitation.permissions, status: 'ACTIVE' }
						})

						await this.invitationFactory.update(
							invitation._id.toString(),
							{ status: 'ACTIVE' },
						context)
					}

					return newUser
				} else {
					throw new Error('Missing or invalid registration data')
				}
			})
		} catch(err) {
			throw new Error(err)
		}
	}
}

export default UserFactory
