import { presence } from 'property-validator'
import path from 'path'
import fsm from 'fs-magic'
import config from 'config'
import { handleDbQuery } from '../handler'
import CollectionFactory from './Factory'
import CustomerFactory from './CustomerFactory'
import UserFactory from './UserFactory'
import FtpConfigFactory from './FtpConfigFactory'

class SoftwareFactory extends CollectionFactory {
	constructor() {
		super()
		this.collectionName = 'softwares'
		this.customerFactory = new CustomerFactory()
		this.userFactory = new UserFactory()
		this.ftpConfigFactory = new FtpConfigFactory()

		this.validationRules = {
			create: [
				presence('softwareId'),
				presence('softwareName')
			],
			update: [
				presence('softwareId'),
				presence('softwareName')
			]
		}
	}

	async isSoftwareRemovable(dirname) {
		const [ files ] = await fsm.scandir(dirname, true, true)
		const uploads = files.filter(name => name.includes('/uploads/'))
		if ( uploads.length ) {
			return false
		}

		return true
	}

	async remove(_id, user, context = null) {
		const ftpConfig = await this.ftpConfigFactory.getOne({ softwareId: _id }, context)
		if (ftpConfig) {
			const dirname = path.join(config.get('service.classifier.directory'), `${context}${ftpConfig.username}`)
			const isSoftwareRemovable = await this.isSoftwareRemovable(dirname)
			if (!isSoftwareRemovable) {
				throw new Error('The directory is not empty')
			}

			await this.ftpConfigFactory.remove(ftpConfig._id.toString(), context, true)
		}

		await super.remove(_id.toString(), context, true)

		const customer = await this.customerFactory.getOne({ context })
		await this.customerFactory.removeChildren(customer._id.toString(), { "softwares": { "$in": [ _id.toString() ] }, "updatedBy": user._id })
	}

	async create(args, user, context) {
		const { softwareId, softwareName } = args
		if(!context || !softwareId || !softwareName) {
			throw new Error('Invalid data')
		}

		try {
			const customer = await this.customerFactory.getOne({ context })
			const softwareData = {
				createdAt: Date.now(),
				createdBy: user._id,
				...args
			}

			const currentUser = await this.userFactory.getOne({ _id: user._id })

			return await handleDbQuery( async db => {
				const softwareExists = await db.collection(this.collectionName).findOne({ softwareId, softwareName, deletedAt: { $exists: false } })
				if (softwareExists) throw new Error('Missing or invalid software data')

				if (currentUser.config.softwarePerCustomer < (customer.softwares.length + 1)) throw new Error('To much software')
				let newSoftware = await super.create(softwareData, context)
				await this.customerFactory.pushChildren(customer._id.toString(), { 'softwares': newSoftware._id.toString(), 'updatedBy': user._id })

				return newSoftware
			}, context )
		} catch (err) {
			throw new Error(err)
		}
	}
}

export default SoftwareFactory
