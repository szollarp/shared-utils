import CollectionFacotry from './Factory'

class NotificationFactory extends CollectionFacotry {
	constructor() {
		super()
		this.collectionName = 'notifications'

		this.validationRules = {
			create: [],
			update: []
		}
	}
}

export default NotificationFactory
