import { presence } from 'property-validator'
import CollectionFacotry from './Factory'

class FtpConfigFactory extends CollectionFacotry {
	constructor() {
		super()
		this.collectionName = 'ftpconfigs'

		this.validationRules = {
			create: [
				presence('softwareId'),
				presence('username'),
				presence('password')
			],
			update: [
				presence('username'),
				presence('password')
			]
		}
	}
}

export default FtpConfigFactory
