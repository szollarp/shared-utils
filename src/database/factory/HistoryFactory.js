import CollectionFacotry from './Factory'

class HistoryFactory extends CollectionFacotry {
	constructor() {
		super()
        this.collectionName = 'history'
        this.historyEnabled = false

		this.validationRules = {
			create: [],
			update: []
		}
	}
}

export default HistoryFactory
