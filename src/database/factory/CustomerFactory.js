import {
	presence
} from 'property-validator';
import config from 'config';
import axios from 'axios';
import {
	generate
} from 'randomstring';

import log from '../../log';
import xml2js from 'xml2js';
import CryptoJS from 'crypto-js';

import {
	handleDbQuery
} from '../index';
import {
	getProviderEndpoint
} from '../../utils';

import CollectionFacotry from './Factory';
import UserFactory from './UserFactory';
import FilterFactory from './FilterFactory';
import InvitationFactory from './InvitationFactory';

import tokenExchangeXML from '../../module/xml-factory/token-exchange';
import {
	TOKEN_ENCRYPTION
} from '../../constants';

import {
	stringToBase64,
	base64ToString
} from '../../module/security';

import {
	handleNotificationWithEmails
} from '../../module/notification';

class CustomerFactory extends CollectionFacotry {
	constructor() {
		super()
		this.collectionName = 'customers'

		this.validationRules = {
			create: [
				presence('taxNumber'),
				presence('name')
			],
			update: [
				presence('taxNumber'),
				presence('name'),
				presence('auth')
			]
		}

		this.userFactory = new UserFactory()
		this.filterFactory = new FilterFactory()
		this.invitationFactory = new InvitationFactory()
	}

	encryptCustomerAuth(auth) {
		if (!auth) return null

		return {
			technicalUserName: auth.technicalUserName.trim(),
			technicalUserPassword: stringToBase64(auth.technicalUserPassword.trim()),
			secretKey: stringToBase64(auth.secretKey.trim()),
			exchangeKey: stringToBase64(auth.exchangeKey.trim())
		}
	}

	async readCustomerConfig(context) {
		const customer = await this.getOne({
			context
		})
		const user = await this.userFactory.getOne({
			_id: customer.createdBy
		})

		const {
			taxNumber,
			auth: {
				technicalUserName,
				technicalUserPassword,
				secretKey,
				exchangeKey
			}
		} = customer

		return ({
			login: technicalUserName,
			password: base64ToString(technicalUserPassword),
			taxNumber,
			signKey: base64ToString(secretKey),
			exchangeKey: base64ToString(exchangeKey),
			primaryEmail: user.email
		})
	}

	tokenExtract(taxNumber, tokenExchangeResponseXML, customerConfig) {
		let decodedExchangeTokenString = null

		try {
			let tokenExchangeResponseXMLObject = null

			xml2js.parseString(tokenExchangeResponseXML, (err, data) => {
				if (err) {
					throw new Error(err)
				} else {
					tokenExchangeResponseXMLObject = data
				}
			})

			const encodedExchangeToken = tokenExchangeResponseXMLObject.TokenExchangeResponse.encodedExchangeToken[0]

			const exchangeKey = CryptoJS.enc.Utf8.parse(customerConfig.exchangeKey)
			const cipherParams = CryptoJS.lib.CipherParams.create({
				ciphertext: CryptoJS.enc.Base64.parse(encodedExchangeToken)
			});

			const decodedExchangeToken = CryptoJS[TOKEN_ENCRYPTION.cipher].decrypt(cipherParams, exchangeKey, {
				mode: CryptoJS.mode[TOKEN_ENCRYPTION.mode],
				padding: CryptoJS.pad.NoPadding
			})

			decodedExchangeTokenString = CryptoJS.enc.Utf8.stringify(decodedExchangeToken);

			decodedExchangeTokenString = decodedExchangeTokenString.replace(/[^a-zA-Z0-9-]/g, "")

		} catch (err) {
			log.error(`Error decrypting token for company with tax number ${taxNumber}: ${err}`)
		}

		return decodedExchangeTokenString
	}

	async isValidCustomerAuth(taxNumber, login, password, signKey, exchangeKey) {
		const customerConfig = {
			login,
			password,
			taxNumber,
			signKey,
			exchangeKey
		}
		let authErrorMessage = null
		log.info(`Validating customer with tax number ${taxNumber}`)

		const tokenExchangeXMLString = tokenExchangeXML(customerConfig)
		const tokenExchangeEndpoint = getProviderEndpoint('TOKEN_EXCHANGE')
		const tokenExchangeResponse = await axios.post(
				tokenExchangeEndpoint,
				tokenExchangeXMLString, {
					headers: {
						'Content-Type': 'application/xml',
						'Accept': 'application/xml'
					}
				}
			)
			.then(response => response)
			.catch(error => {
				authErrorMessage = 'Invalid customer data. Please check your core tax number, technical username/password, secret key, or exchange key'
				return error.response
			})

		if (!tokenExchangeResponse || !tokenExchangeResponse.data.startsWith('<?xml')) {
			authErrorMessage = 'HU TA service is temporarily unavailable, please, try again later'
		}

		if (authErrorMessage) {
			return authErrorMessage
		}

		const {
			data
		} = tokenExchangeResponse

		log.info(`Validating result of customer with tax number ${taxNumber}: ${data}`)

		const decryptedToken = this.tokenExtract(taxNumber, data, customerConfig)
		if (!decryptedToken) {
			authErrorMessage = 'Could not decode token from HU TA, please, check your exchange key'
		}

		return (authErrorMessage || true)
	}

	async create(args, user) {
		const {
			name,
			taxNumber,
			seatPostcode,
			seatTown,
			seatAddress,
			seatCountry,
			auth
		} = args

		if (!name || !taxNumber || !seatPostcode || !seatTown || !seatAddress || !seatCountry) {
			throw new Error('Invalid data')
		} else {
			if (auth && (!auth.technicalUserName || !auth.technicalUserPassword || !auth.exchangeKey || !auth.secretKey)) {
				throw new Error('Invalid data')
			}
		}

		try {
			const usersExistingCompanyWithTaxNumber = await this.getOne({
				createdBy: user._id,
				taxNumber
			})
			if (usersExistingCompanyWithTaxNumber) {
				throw new Error(`Duplicated tax number. This Tax number has already been registrated at ${usersExistingCompanyWithTaxNumber.name}`)
			}

			let encryptedAuth = {}
			if (auth && auth.technicalUserName && auth.technicalUserPassword && auth.exchangeKey && auth.secretKey) {
				const validationResult = await this.isValidCustomerAuth(
					taxNumber.trim(),
					auth.technicalUserName.trim(),
					auth.technicalUserPassword.trim(),
					auth.secretKey.trim(),
					auth.exchangeKey.trim()
				)

				if (validationResult !== true) {
					throw new Error(validationResult)
				}

				encryptedAuth = this.encryptCustomerAuth(auth)
			}
			const currentUser = await this.userFactory.getOne({
				_id: user._id
			})

			const context = generate(15)
			const customerData = {
				createdAt: Date.now(),
				createdBy: user._id,
				context,
				auth: encryptedAuth,
				name,
				taxNumber,
				seatCountry,
				seatPostcode,
				seatTown,
				seatAddress,
				softwares: [],
				notifications: config.get('defaults.customer').notifications(currentUser.email),
				features: config.get('defaults.customer.features')
			}

			const newCustomer = await handleDbQuery(async () => {
				const customerExists = null
				if (!customerExists) {
					return await super.create(customerData)
				} else {
					throw new Error('Missing or invalid customer data')
				}
			})

			if (config.has('defaults.customer.filters')) {
				config.get('defaults.customer.filters').filters.map(async filter => {
					await this.filterFactory.create(filter, user, context)
				})
			}

			const {
				supportEmail,
				sendWithRegistration,
				permissions,
				status
			} = config.get('defaults.customer.invitation')
			if (supportEmail && sendWithRegistration && supportEmail !== currentUser.email) {
				const supportUser = await this.userFactory.getOne({
					email: supportEmail
				})
				if (supportUser) {
					await this.invitationFactory.create({
						email: supportEmail,
						permissions,
						status,
						createdBy: currentUser._id,
						userIsExisted: true,
					}, context)

					await this.userFactory.pushChildren(supportUser._id.toString(), {
						permissions: {
							context,
							...permissions,
							status
						}
					})

					await handleNotificationWithEmails('IRINA_INVITATION_SUCCESS', {
						taxNumber: newCustomer.taxNumber,
						name: newCustomer.name,
						userName: currentUser.lastName + ' ' + currentUser.firstName
					}, [supportEmail])
				}
			}

			return newCustomer
		} catch (err) {
			throw new Error(err)
		}
	}

	async update(id, args, user) {
		let {
			taxNumber,
			auth
		} = args
		let encryptedAuth = {}

		try {
			const customer = await super.getOne({
				_id: id
			})
			if (!customer) {
				throw new Error('Customer not exists')
			}
			if (user._id.toString() !== customer.createdBy.toString()) {
				taxNumber = customer.taxNumber
				args.taxNumber = taxNumber
			}

			if (auth && auth.technicalUserName && auth.technicalUserPassword && auth.exchangeKey && auth.secretKey) {
				const validationResult = await this.isValidCustomerAuth(
					taxNumber.trim(),
					auth.technicalUserName.trim(),
					auth.technicalUserPassword.trim(),
					auth.secretKey.trim(),
					auth.exchangeKey.trim()
				)
				if (validationResult !== true) {
					throw new Error(validationResult)
				}

				encryptedAuth = this.encryptCustomerAuth(auth)
			}

			const customerData = {}
			for (const key of Object.keys(args)) {
				if (key !== 'id' && ((!args[key] && args[key] !== '') || Object.keys(args[key]))) {
					customerData[key] = args[key]
				}
			}

			if (encryptedAuth.technicalUserName) {
				customerData.auth = encryptedAuth
			}

			delete args.id
			await super.update(id, { ...customerData,
				updatedBy: user._id
			})
			return this.get({
				createdBy: user._id
			})
		} catch (err) {
			throw new Error(err)
		}
	}
}

export default CustomerFactory