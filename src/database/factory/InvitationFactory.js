import CollectionFacotry from './Factory'
import { presence, email, validate } from 'property-validator'

class InvitationFactory extends CollectionFacotry {
	constructor() {
		super()
		this.collectionName = 'invitations'

		this.validationRules = {
			create: [
				presence('permissions'),
				presence('status'),
				email('email')
			]
		}
	}

	async create(args, context) {
		try {
			const validation = validate(args, this.validationRules.create)
			if(!validation.valid || !context) {
				throw new Error('Invalid data')
			}

			const isSet = await this.getOne({ email: args.email }, context)
			if(isSet) {
				throw new Error('The user has been already invited. Please click on Resend invitation to send the email again.')
			}

			return await super.create(args, context)
		} catch (err) {
			throw new Error(err)
		}
	}
}

export default InvitationFactory
