import CollectionFacotry from './Factory'

class TrackedInvoiceFactory extends CollectionFacotry {
	constructor() {
		super()
		this.collectionName = 'trackedinvoices'

		this.validationRules = {
			create: [],
			update: []
		}
	}
}

export default TrackedInvoiceFactory
