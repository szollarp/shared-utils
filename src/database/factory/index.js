import UserFactory from './UserFactory';
import CustomerFactory from './CustomerFactory';
import SoftwareFactory from './SoftwareFactory';
import FtpConfigFactory from './FtpConfigFactory';
import InvoiceReportFactory from './InvoiceReportFactory';
import NotificationFactory from './NotificationFactory';
import IssueFactory from './IssueFactory';
import FilterFactory from './FilterFactory';
import InvitationFactory from './InvitationFactory';
import HistoryFactory from './HistoryFactory';
import KeyFactory from './KeyFactory';
import ReconciliationListFactory from './ReconciliationListFactory';
import TrackedInvoiceFactory from './TrackedInvoiceFactory';

export default {
	UserFactory,
	CustomerFactory,
	SoftwareFactory,
	FtpConfigFactory,
	InvoiceReportFactory,
	NotificationFactory,
	IssueFactory,
	FilterFactory,
	InvitationFactory,
	HistoryFactory,
	KeyFactory,
	ReconciliationListFactory,
	TrackedInvoiceFactory
}
