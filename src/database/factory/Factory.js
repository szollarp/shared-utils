import {
	ObjectID
} from 'mongodb';
import History from '../../module/history';
import {
	HistoryCode
} from '../../constants';
import {
	handleDbQuery
} from '../handler';

class CollectionFactory {
	constructor() {
		this.collectionName = null;
		this.historyEnabled = true;
		this.validationRules = {
			create: [],
			update: []
		};
	}

	isHistoryEnabled = () =>
		this.historyEnabled && this.collectionName !== 'invoicereports';

	isOnlyHistoryAdded(query) {
		let keys = Object.keys(query);
		let key = keys[0];

		return (keys.length === 1 && key === "history");
	}

	async create(query, context = null) {
		if (!query) return null

		try {
			return await handleDbQuery(async db => {
				const newObject = await db.collection(this.collectionName).insertOne({ ...query,
					createdAt: Date.now()
				});

				const objectCreated = Array.isArray(newObject.ops) ? newObject.ops.pop() : newObject.ops;

				if (this.isHistoryEnabled()) {
					await History(this.collectionName, objectCreated, HistoryCode.ELEMENT_CREATED, query, context, query.createdBy)
				};

				return objectCreated;
			}, context)
		} catch (err) {
			throw new Error(err);
		}
	}

	async update(_id, query, context = null) {
		if (!_id) return null
		_id = ObjectID.createFromHexString(_id);

		let updatedAt = this.isOnlyHistoryAdded(query) ? null : Date.now();
		let setObject = { ...query
		};
		if (updatedAt) {
			setObject.updatedAt = updatedAt
		};

		delete setObject.id;
		delete setObject._id;

		try {
			return await handleDbQuery(async db => {
				const response = await db.collection(this.collectionName).findOneAndUpdate({
					_id
				}, {
					$set: setObject
				}, {
					returnOriginal: false
				})

				if (response.ok) {
					const updatedObject = response.value

					if (this.isHistoryEnabled()) {
						await History(this.collectionName, updatedObject, HistoryCode.ELEMENT_UPDATED, query, context, query.updatedBy)
					}

					return updatedObject
				} else {
					throw new Error('Error during update')
				};

			}, context);
		} catch (err) {
			throw new Error(err);
		}
	}

	async get(query, context = null, operations = {}, orderBy = {}) {
		if (query._id && typeof query._id !== ObjectID) {
			query._id = ObjectID.createFromHexString(query._id);
		}

		try {
			return await handleDbQuery(async db => {
				let results = await db.collection(this.collectionName).find(query);
				if (orderBy) results = await results.sort(orderBy);
				if (operations.skip) results = await results.skip(operations.skip);
				if (operations.limit) results = await results.limit(operations.limit);

				return results.toArray();

			}, context);
		} catch (err) {
			throw new Error(err);
		}
	}

	async getByIds(idsArray, context = null, orderBy = {}) {
		idsArray = idsArray.map((id) => {
			if (typeof id !== ObjectID) {
				id = ObjectID.createFromHexString(id);
			}
			return id;
		})

		try {
			return await handleDbQuery(async db => {
				let objects = await db.collection(this.collectionName).find({
					_id: {
						$in: idsArray
					}
				});
				if (orderBy) {
					objects = await objects.sort(orderBy);
				}

				return objects.toArray();
			}, context)
		} catch (err) {
			throw new Error(err);
		}
	}

	async getLimited(query, limit, context = null) {
		if (query._id && typeof query._id !== ObjectID) {
			query._id = ObjectID.createFromHexString(query._id);
		}

		try {
			return await handleDbQuery(async db => (
				await db.collection(this.collectionName).find(query).limit(limit).toArray()
			), context);
		} catch (err) {
			throw new Error(err);
		}
	}

	async getOneWithFields(query, fields, context = null) {
		if (query._id && typeof query._id !== ObjectID) {
			query._id = ObjectID.createFromHexString(query._id);
		}

		try {
			const response = await handleDbQuery(async db => (
				await db.collection(this.collectionName).find(query, fields).toArray()
			), context);

			return response.pop();
		} catch (err) {
			throw new Error(err);
		}
	}

	async getOne(query, context = null) {
		const response = await this.get(query, context);
		return response.pop();
	}

	async removeChildren(_id, pull, context) {
		if (!_id) return pull;
		_id = ObjectID.createFromHexString(_id);

		const userId = (pull.createdBy || pull.updatedBy || null);

		delete pull['updatedBy'];
		delete pull['createdBy'];

		try {
			return await handleDbQuery(async db => {
				const response = await db.collection(this.collectionName).findOneAndUpdate({
					_id
				}, {
					$pull: { ...pull
					}
				}, {
					returnOriginal: false,
					multi: true
				});

				if (response.ok) {
					const {
						value
					} = response;

					if (this.isHistoryEnabled()) {
						await History(this.collectionName, value, HistoryCode.ELEMENT_CHILDREN_PULLED, JSON.stringify(pull), context, userId);
					}

					return value;
				} else {
					throw new Error('Error during update');
				}
			}, context)
		} catch (err) {
			throw new Error(err);
		}
	}

	async pushChildren(_id, push, context) {
		if (!_id) return null;
		_id = ObjectID.createFromHexString(_id);

		const userId = (push.createdBy || push.updatedBy || null);

		delete push['updatedBy'];
		delete push['createdBy'];

		try {
			return await handleDbQuery(async db => {
				const response = await db.collection(this.collectionName).findOneAndUpdate({
					_id
				}, {
					$push: { ...push
					}
				}, {
					returnOriginal: false
				});

				if (response.ok) {
					const {
						value
					} = response;
					if (this.isHistoryEnabled()) {
						await History(this.collectionName, value, HistoryCode.ELEMENT_CHILDREN_PUSHED, push, context, userId);
					}

					return value;
				} else {
					throw new Error('Error during update')
				}
			}, context)
		} catch (err) {
			throw new Error(err)
		}
	}

	async remove(_id, context = null, soft = false) {
		if (!_id) return null;
		_id = ObjectID.createFromHexString(_id);

		try {
			return await handleDbQuery(async db => {
				if (!soft) {
					return await db.collection(this.collectionName).deleteOne({
						_id
					});
				}

				if (soft) {
					return await db.collection(this.collectionName).findOneAndUpdate({
						_id
					}, {
						$set: {
							deletedAt: Date.now()
						}
					}, {
						returnOriginal: false
					});
				}
			}, context);
		} catch (err) {
			throw new Error(err)
		}
	}

	async count(query, context = null) {
		try {
			return await handleDbQuery(async db => {
				const count = await db.collection(this.collectionName).find(query).count();
				return count;
			}, context);
		} catch (err) {
			throw new Error(err);
		}
	}

	async aggregate(query, context = null) {
		try {
			return await handleDbQuery(async db => {
				const agreatetedData = await db.collection(this.collectionName).aggregate(query);
				return agreatetedData.toArray();
			}, context);
		} catch (err) {
			throw new Error(err)
		}
	}
}

export default CollectionFactory;