import xml2js from 'xml2js'
import config from 'config'
import {
	IssueCodePriorities,
	IssueCodesToIgnore,
	moduleNames
} from '../../constants'
import CollectionFactory from './Factory'
import InvoiceReportFactory from './InvoiceReportFactory'
import CustomerFactory from './CustomerFactory'
import {
	handleNotification,
	handleNotificationWithEmails
} from '../../module/notification'

class IssueFactory extends CollectionFactory {
	constructor(createdByModule) {
		super()
		this.collectionName = 'issues'
		this.invoiceReportFactory = new InvoiceReportFactory()
		this.customerFactory = new CustomerFactory()
		this.adminEmails = config.get('email.mailer.toAddresses')
		this.createdByModule = createdByModule || moduleNames.REPORTING

		this.validationRules = {
			create: [],
			update: []
		}
	}

	async create(issuingObject, invoiceReportId, context) {
		if (!invoiceReportId) return null

		const issue = {
			code: 'UNKNOWN'
		}
		const response = issuingObject.response
		let returnIssue = null

		try {
			if (response && response.request && response.data && response.config) {
				const xmlData = response.data
				let xmlDataObject = null

				if (xmlData.startsWith('<html>')) {
					return returnIssue
				}

				xml2js.parseString(xmlData, (err, data) => {
					if (err) {
						throw new Error(err)
					}

					xmlDataObject = data
				})

				if (xmlDataObject.GeneralErrorResponse) {
					issue.code = xmlDataObject.GeneralErrorResponse.result[0].errorCode[0]
				} else if (xmlDataObject.GeneralExceptionResponse) {
					issue.code = xmlDataObject.GeneralExceptionResponse.errorCode[0]
				}

			} else {
				issue.code = issuingObject.code
			}

			if (IssueCodesToIgnore.includes(issue.code)) {
				return null
			}

			issue.priority = IssueCodePriorities['HIGH'].includes(issue.code) ? 'HIGH' :
				(IssueCodePriorities['LOW'].includes(issue.code) ? 'LOW' : null)

			if (!issue.priority && issue.code) {
				await handleNotificationWithEmails('UNREGISTERED_ISSUE_CODE', issue, this.adminEmails)
			}

			if (issue.priority && issue.code) {
				issue.description = issue.code

				const customer = await this.customerFactory.getOne({
					context
				})
				if (!customer) {
					throw new Error('Invalid customer')
				}

				const existingToDoIssue = await this.getOne({
					status: 'TODO',
					code: issue.code
				}, context)
				const existingIssueWithThisInvoice = await this.getOne({
					"code": issue.code,
					"invoiceReports.id": invoiceReportId.toString()
				}, context)
				const existingInvoice = await this.invoiceReportFactory.getOne({
					_id: invoiceReportId
				}, context)
				if (!existingInvoice) {
					throw new Error(`InvoiceReport with id '${invoiceReportId}' does not exists in customer's (${context}) DB`)
				}
				const invoiceReportForIssue = {
					id: existingInvoice._id.toString(),
					invoiceNumber: existingInvoice.invoiceNumber,
					source: existingInvoice.source,
					filename: existingInvoice.filename
				}

				if (!existingIssueWithThisInvoice) {
					if (existingToDoIssue) {
						returnIssue = await this.pushChildren(existingToDoIssue._id.toString(), {
							'invoiceReports': invoiceReportForIssue
						}, context)
					} else {
						const issueData = {
							...issue,
							createdBy: customer.createdBy,
							createdByModule: this.createdByModule,
							status: 'TODO',
							invoiceReports: [invoiceReportForIssue]
						}

						returnIssue = await super.create(issueData, context)
					}
				}
			}

			if (returnIssue) {
				await handleNotification('ISSUE', returnIssue, context)
			}

		} catch (error) {
			throw new Error(error)
		}

		return returnIssue
	}

	async createForFilesOnly(issuingObject, filename, context) {
		if (!filename) return null

		const issue = {
			code: 'UNKNOWN'
		}
		const response = issuingObject.response
		let returnIssue = null

		try {
			if (response && response.request && response.data && response.config) {
				const xmlData = response.data
				let xmlDataObject = null

				if (xmlData.startsWith('<html>')) {
					return returnIssue
				}

				xml2js.parseString(xmlData, (err, data) => {
					if (err) {
						throw new Error(err)
					}

					xmlDataObject = data
				})

				if (xmlDataObject.GeneralErrorResponse) {
					issue.code = xmlDataObject.GeneralErrorResponse.result[0].errorCode[0]
				} else if (xmlDataObject.GeneralExceptionResponse) {
					issue.code = xmlDataObject.GeneralExceptionResponse.errorCode[0]
				}

			} else {
				issue.code = issuingObject.code
			}

			/* if (IssueCodesToIgnore.includes(issue.code)) {
				return null
			} */

			issue.priority = IssueCodePriorities['HIGH'].includes(issue.code) ? 'HIGH' :
				(IssueCodePriorities['LOW'].includes(issue.code) ? 'LOW' : null)

			if (!issue.priority && issue.code) {
				await handleNotificationWithEmails('UNREGISTERED_ISSUE_CODE', issue, this.adminEmails)
			}

			if (issue.priority && issue.code) {
				issue.description = issue.code

				const customer = await this.customerFactory.getOne({
					context
				})
				if (!customer) {
					throw new Error('Invalid customer')
				}

				const existingToDoIssue = await this.getOne({
					status: 'TODO',
					code: issue.code
				}, context)
				const existingIssueWithThisFile = await this.getOne({
					"code": issue.code,
					"files.filename": filename
				}, context)
				const fileForIssue = {
					source: '',
					filename: filename
				}

				if (!existingIssueWithThisFile) {
					if (existingToDoIssue) {
						returnIssue = await this.pushChildren(existingToDoIssue._id.toString(), {
							'files': fileForIssue
						}, context)
					} else {
						const issueData = {
							...issue,
							createdBy: customer.createdBy,
							createdByModule: this.createdByModule,
							status: 'TODO',
							files: [fileForIssue]
						}

						returnIssue = await super.create(issueData, context)
					}
				} else {
					returnIssue = existingIssueWithThisFile
				}
			}

			if (returnIssue) {
				await handleNotification('ISSUE', returnIssue, context)
			}

		} catch (error) {
			throw new Error(error)
		}

		return returnIssue
	}

	async createForInvoicesOnly(issuingObject, invoiceNumber, context, source = '') {
		if (!invoiceNumber) return null

		const issue = {
			code: 'UNKNOWN'
		}
		let returnIssue = null

		try {
			issue.code = issuingObject.code

			if (IssueCodesToIgnore.includes(issue.code)) {
				return null
			}

			issue.priority = IssueCodePriorities['HIGH'].includes(issue.code) ? 'HIGH' :
				(IssueCodePriorities['LOW'].includes(issue.code) ? 'LOW' : null)

			if (!issue.priority && issue.code) {
				await handleNotificationWithEmails('UNREGISTERED_ISSUE_CODE', issue, this.adminEmails)
			}

			if (issue.priority && issue.code) {
				issue.description = issue.code

				const customer = await this.customerFactory.getOne({
					context
				})
				if (!customer) {
					throw new Error('Invalid customer')
				}

				const invoiceForIssue = {
					source,
					invoiceNumber
				}
				const existingToDoIssue = await this.getOne({
					status: 'TODO',
					code: issue.code
				}, context)
				const existingIssueWithThisInvoice = await this.getOne({
					"code": issue.code,
					invoices: invoiceForIssue
				}, context)
				if (!existingIssueWithThisInvoice) {
					if (existingToDoIssue) {
						returnIssue = await this.pushChildren(existingToDoIssue._id.toString(), {
							'invoices': invoiceForIssue
						}, context)
					} else {
						const issueData = {
							...issue,
							createdBy: customer.createdBy,
							createdByModule: this.createdByModule,
							status: 'TODO',
							invoices: [invoiceForIssue]
						}

						returnIssue = await super.create(issueData, context)
					}
				}
			}

			if (returnIssue) {
				await handleNotification('ISSUE', returnIssue, context)
			}

		} catch (error) {
			throw new Error(error)
		}

		return returnIssue
	}
}

export default IssueFactory