import module from './module';
import database from './database';
import resource from './resource';

export default {
    module,
    database,
    resource,
};