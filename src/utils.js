import config from 'config';
import {
    providerEndpoints
} from './constants';

export const getProviderEndpoint = endpoint =>
    `${config.get('general.provider')}/${providerEndpoints[endpoint]}`