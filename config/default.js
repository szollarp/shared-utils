module.exports = {
    general: {
        debugMode: true,
        folder: {
            watched: './tmp/xml'
        },
        provider: 'https://api-test.onlineszamla.nav.gov.hu'
    },
    service: {
        classifier: {
            directory: './tmp/xml',
            rules: {
                isUploads: /(\/uploads\/)/,
                isOutbound: /(\/outbound\/)/,
                isReconciliation: /(\/reconciliation\/)/,
                isInvoiceReport: /(\/invoicereports\/)/,
                notContainsReconciliation: /^(?![a-zA-z0-9\-.]*[-]{0,1}reconciliation.xml).*$/,
                notContainsDotInFirst: /^[^.].*/
            }
        },
        invoiceHandler: {
            queueConfig: {
                queueName: 'invoiceHandler',
                nextQueueName: 'needToRule'
            }
        },
        ruler: {
            queueConfig: {
                queueName: 'needToRule',
                nextQueueName: 'needToSend'
            }
        },
        sender: {
            queueConfig: {
                queueName: 'needToSend',
                nextQueueName: 'needToReceive'
            }
        },
        receiver: {
            queueConfig: {
                queueName: 'needToReceive'
            }
        },
        notifier: {
            queueConfig: {
                queueName: 'needToNotify',
            },
        },
        reconciliationList: {
            handler: {
                watcherConfig: {
                    directory: './tmp/xml'
                },
                queueConfig: {
                    queueName: 'reconciliationListFiles',
                    nextQueueName: 'needToFetch'
                }
            },
            fetcher: {
                queueConfig: {
                    queueName: 'needToFetch',
                    nextQueueName: 'needToProcess'
                },
                daysInDateIntervals: 35,
                dateFormat: 'YYYY-MM-DD'
            },
            processor: {
                queueConfig: {
                    queueName: 'needToProcess'
                },
                minVatValue: 100000
            }
        },
        api: {
            port: 9004
        }
    },
    queue: {
        general: {
            host: '127.0.0.1',
            port: 6379,
            ns: 'rsmq'
        },
        worker: {
            interval: [0, 1, 2, 3, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
            alwaysLogErrors: true,
            maxReceiveCount: 10
        }
    },
    database: {
        host: 'mongodb://127.0.0.1',
        database: 'rsmrtir_dev'
    },
    auth: {
        tokenExpires: 5 * 60,
        refreshTokenExpires: 2 * 60 * 60,
        jwtSecret: 'sdfgrwf4guozfhkg4uefowhk',
        jwtRefreshSecret: 'sripjeshiuno4jwrfsinupjoqenispjw'
    },
    email: {
        uiHost: 'http://127.0.0.1',
        mailer: {
            endpoint: 'http://rsm5gapi.dotin.hu/v1/mailers/sendmail',
            smtpHost: 'localhost',
            fromAddress: 'no-reply@connectax-local.com',
            toAddresses: ['szollarp@gmail.com'],
            subject: 'Notification from RSM ConnecTax',
            headerInfo: {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            }
        }
    },
    defaults: {
        licences: {
            asInvite: 0,
            asRegistration: 1
        },
        customer: {
            filters: [{
                    name: 'Without tax ID',
                    enabled: false,
                    conditions: [{
                        fieldName: 'invoiceHead.customerInfo.customerTaxNumber.taxpayerId',
                        comparer: 'IS_NOT_PROVIDED',
                        comparedValue: ''
                    }]
                },
                {
                    name: 'Below the threshold',
                    enabled: false,
                    conditions: [{
                        fieldName: "vatAmountHUF",
                        comparer: "LESS_THAN",
                        comparedValue: "100000"
                    }, {
                        fieldName: "hasAdvanceItems",
                        comparer: "IS_FALSE",
                        comparedValue: ''
                    }, {
                        fieldName: "isCorrectionInvoice",
                        comparer: "IS_FALSE",
                        comparedValue: ''
                    }]
                },
                {
                    name: 'Without invoice number',
                    enabled: false,
                    conditions: [{
                        fieldName: "invoiceHead.invoiceData.invoiceNumber",
                        comparer: "IS_NOT_PROVIDED",
                        comparedValue: ''
                    }]
                }
            ],
            notifications(email) {
                return [{
                    type: 'high',
                    on: true,
                    emails: [email]
                }, {
                    type: 'low',
                    on: true,
                    emails: [email]
                }, {
                    type: 'done',
                    on: false,
                    emails: []
                }]
            },
            invitation: {
                sendWithRegistration: true,
                supportEmail: 'support@demo.com',
                permissions: {
                    write: false,
                    read: true
                },
                status: 'ACTIVE'
            },
            features: {
                archiving: false
            }
        }
    }
}