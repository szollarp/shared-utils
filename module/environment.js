"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hasUploadsFolder = void 0;

var _require = require('fs'),
    lstatSync = _require.lstatSync,
    readdirSync = _require.readdirSync,
    existsSync = _require.existsSync;

var _require2 = require('path'),
    join = _require2.join,
    sep = _require2.sep;

var isDirectory = function isDirectory(file) {
  if (!existsSync(file)) {
    return false;
  }

  return lstatSync(file).isDirectory();
};

var getDirectories = function getDirectories(file) {
  return readdirSync(file).map(function (name) {
    return join(file, name);
  }).filter(isDirectory);
};

var getFfpUsernameFromFileName = function getFfpUsernameFromFileName(fileName) {
  var schemas = {
    small: fileName.match(/(\/)([a-zA-Z0-9]{25})(\/)/i),
    medium: fileName.match(/(\/)([a-zA-Z0-9]{31})(\/)/i)
  };

  if (schemas.small && schemas.small[2] || schemas.medium && schemas.medium[2]) {
    return schemas.small ? schemas.small[2] : schemas.medium[2];
  }

  return false;
};

var hasUploadsFolder = function hasUploadsFolder(directory, file) {
  var userDirectories = getDirectories(directory);
  var ftpUsername = getFfpUsernameFromFileName(file);

  if (!ftpUsername) {
    return false;
  }

  var userDirectory = userDirectories.filter(function (dirPath) {
    var username = dirPath.split(sep).pop();
    return username === ftpUsername;
  }).pop();
  var uploadDirectories = getDirectories(userDirectory);
  var uploadsDirectory = uploadDirectories.filter(function (uploadDir) {
    var dirname = uploadDir.split(sep).pop();
    return dirname === 'uploads';
  });
  return uploadsDirectory.length > 0;
};

exports.hasUploadsFolder = hasUploadsFolder;