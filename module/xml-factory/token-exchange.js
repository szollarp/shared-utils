"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _xml2js = _interopRequireDefault(require("xml2js"));

var _moment = _interopRequireDefault(require("moment"));

var _jsSha = _interopRequireDefault(require("js-sha512"));

var _tokenExchange = _interopRequireDefault(require("../../resource/xml/token-exchange"));

var _default = function _default(customerConfig, invoicingSoftwareObject) {
  var generatedXML = null;

  try {
    var providerXMLTemplateObject = null;

    _xml2js.default.parseString(_tokenExchange.default, function (err, data) {
      if (err) {
        throw new Error(err);
      } else {
        providerXMLTemplateObject = data;
      }
    });

    var requestId = 'RID' + new Date().getTime();
    providerXMLTemplateObject.TokenExchangeRequest.header[0].requestId = requestId;

    var timeStringNow = _moment.default.utc().format('YYYY-MM-DDTHH:mm:ss.SSS');

    var timeStringNowZoned = timeStringNow + 'Z';
    var timeStringNowFormatted = (0, _moment.default)(timeStringNow).format('YYYYMMDDHHmmss');
    providerXMLTemplateObject.TokenExchangeRequest.header[0].timestamp = timeStringNowZoned;
    var login = customerConfig.login;
    var passwordHash = (0, _jsSha.default)(customerConfig.password).toUpperCase();
    providerXMLTemplateObject.TokenExchangeRequest.user[0].login = login;
    providerXMLTemplateObject.TokenExchangeRequest.user[0].passwordHash = passwordHash;
    providerXMLTemplateObject.TokenExchangeRequest.user[0].taxNumber = customerConfig.taxNumber;
    var requestSignature = (0, _jsSha.default)(requestId + timeStringNowFormatted + customerConfig.signKey).toUpperCase();
    providerXMLTemplateObject.TokenExchangeRequest.user[0].requestSignature = requestSignature;
    providerXMLTemplateObject.TokenExchangeRequest.software.length = 0;

    if (invoicingSoftwareObject) {
      providerXMLTemplateObject.TokenExchangeRequest.software.push({});

      if (invoicingSoftwareObject.softwareId) {
        providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareId = invoicingSoftwareObject.softwareId;
      }

      if (invoicingSoftwareObject.softwareName) {
        providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareName = invoicingSoftwareObject.softwareName;
      }

      if (invoicingSoftwareObject.softwareOperation) {
        providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareOperation = invoicingSoftwareObject.softwareOperation;
      }

      if (invoicingSoftwareObject.softwareMainVersion) {
        providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareMainVersion = invoicingSoftwareObject.softwareMainVersion;
      }

      if (invoicingSoftwareObject.softwareDevName) {
        providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareDevName = invoicingSoftwareObject.softwareDevName;
      }

      if (invoicingSoftwareObject.softwareDevContact) {
        providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareDevContact = invoicingSoftwareObject.softwareDevContact;
      }

      if (invoicingSoftwareObject.softwareDevCountryCode) {
        providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareDevCountryCode = invoicingSoftwareObject.softwareDevCountryCode;
      }

      if (invoicingSoftwareObject.softwareDevTaxNumber) {
        providerXMLTemplateObject.TokenExchangeRequest.software[0].softwareDevTaxNumber = invoicingSoftwareObject.softwareDevTaxNumber;
      }
    }

    var xmlBuilder = new _xml2js.default.Builder();
    generatedXML = xmlBuilder.buildObject(providerXMLTemplateObject);
  } catch (err) {
    throw new Error(err);
  }

  return generatedXML;
};

exports.default = _default;