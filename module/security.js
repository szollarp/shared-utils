"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.base64ToString = exports.stringToBase64 = void 0;

var _buffer = require("buffer");

var stringToBase64 = function stringToBase64(string) {
  return new _buffer.Buffer(string, 'utf8').toString('base64');
};

exports.stringToBase64 = stringToBase64;

var base64ToString = function base64ToString(base64) {
  return new _buffer.Buffer(base64, 'base64').toString('utf8');
};

exports.base64ToString = base64ToString;