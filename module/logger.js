"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _winston = _interopRequireDefault(require("winston"));

var messageFormat = _winston.default.format.printf(function (info) {
  return "".concat(info.timestamp, " [").concat(info.level, "] ").concat(info.message);
});

var logger = _winston.default.createLogger({
  levels: _winston.default.config.syslog.levels,
  format: _winston.default.format.combine(_winston.default.format.timestamp(), messageFormat),
  transports: [new _winston.default.transports.Console({
    format: _winston.default.format.combine(_winston.default.format.colorize(), _winston.default.format.timestamp(), messageFormat)
  })]
});

var _default = logger;
exports.default = _default;