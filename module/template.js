"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createMailContentByNotification = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _camelcase = _interopRequireDefault(require("camelcase"));

var _CustomerFactory = _interopRequireDefault(require("../database/factory/CustomerFactory"));

var createMailContentByNotification =
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(notificationObject) {
    var lang, notificationType, templateModuleName, templateModule, mailContent, customerName, templateModules, customerFactory, customer;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            lang = 'en';
            notificationType = notificationObject.type;
            templateModuleName = (0, _camelcase.default)(notificationType);
            templateModuleName = templateModuleName.replace('irina', '');
            templateModuleName = templateModuleName.charAt(0).toLowerCase() + templateModuleName.slice(1);
            templateModule = null;
            mailContent = null;
            customerName = '';
            _context.prev = 8;
            templateModules = require("../../resources/email/template/".concat(lang));
            templateModule = templateModules[templateModuleName];

            if (templateModule) {
              _context.next = 13;
              break;
            }

            throw new Error("no template found with name '".concat(templateModuleName, "'"));

          case 13:
            if (!notificationObject.customerHash) {
              _context.next = 19;
              break;
            }

            customerFactory = new _CustomerFactory.default();
            _context.next = 17;
            return customerFactory.getOne({
              context: notificationObject.customerHash
            });

          case 17:
            customer = _context.sent;
            customerName = customer && customer.name ? customer.name : '';

          case 19:
            if (!notificationObject.payload.customerName && notificationObject.type !== 'CUSTOM_NOTIFICATIONS') {
              notificationObject.payload.customerName = customerName;
            }

            mailContent = templateModule(notificationObject.payload);
            _context.next = 26;
            break;

          case 23:
            _context.prev = 23;
            _context.t0 = _context["catch"](8);
            throw new Error(_context.t0);

          case 26:
            return _context.abrupt("return", mailContent);

          case 27:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[8, 23]]);
  }));

  return function createMailContentByNotification(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.createMailContentByNotification = createMailContentByNotification;