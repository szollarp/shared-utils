"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.handleNotificationWithEmails = exports.handleNotification = void 0;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/objectSpread"));

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _circularJson = _interopRequireDefault(require("circular-json"));

var _config = _interopRequireDefault(require("config"));

var _constants = require("../constants");

var _queue = require("./queue");

var queueName = get.config('service.notifier.queueConfig.queueName');

var handleNotification =
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(type, payload) {
    var customerHash,
        sendIdOnly,
        entityType,
        notification,
        message,
        _args = arguments;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            customerHash = _args.length > 2 && _args[2] !== undefined ? _args[2] : null;
            sendIdOnly = false;
            _context.t0 = _regenerator.default.keys(_constants.notificationCodesToPassQueueById);

          case 3:
            if ((_context.t1 = _context.t0()).done) {
              _context.next = 11;
              break;
            }

            entityType = _context.t1.value;

            if (!_constants.notificationCodesToPassQueueById[entityType].includes(type)) {
              _context.next = 8;
              break;
            }

            sendIdOnly = true;
            return _context.abrupt("break", 11);

          case 8:
            ;
            _context.next = 3;
            break;

          case 11:
            ;
            notification = {
              type: type,
              payload: sendIdOnly ? {
                _id: payload._id
              } : payload,
              customerHash: customerHash
            };
            message = _circularJson.default.stringify(notification);
            _context.next = 16;
            return (0, _queue.sendMessageToQueue)(queueName, message);

          case 16:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function handleNotification(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.handleNotification = handleNotification;

var handleNotificationWithEmails =
/*#__PURE__*/
function () {
  var _ref2 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee2(type, payload) {
    var emails,
        sendIdOnly,
        entityType,
        notificationObject,
        message,
        _args2 = arguments;
    return _regenerator.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            emails = _args2.length > 2 && _args2[2] !== undefined ? _args2[2] : [];
            sendIdOnly = false;
            _context2.t0 = _regenerator.default.keys(_constants.notificationCodesToPassQueueById);

          case 3:
            if ((_context2.t1 = _context2.t0()).done) {
              _context2.next = 11;
              break;
            }

            entityType = _context2.t1.value;

            if (!_constants.notificationCodesToPassQueueById[entityType].includes(type)) {
              _context2.next = 8;
              break;
            }

            sendIdOnly = true;
            return _context2.abrupt("break", 11);

          case 8:
            ;
            _context2.next = 3;
            break;

          case 11:
            ;
            payload = sendIdOnly ? {
              _id: payload._id
            } : payload;
            notificationObject = {
              type: type,
              payload: (0, _objectSpread2.default)({}, payload, {
                uiHost: _config.default.get('email.uiHost')
              }),
              emails: emails
            };
            message = _circularJson.default.stringify(notificationObject);
            _context2.next = 17;
            return (0, _queue.sendMessageToQueue)(queueName, message);

          case 17:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function handleNotificationWithEmails(_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

exports.handleNotificationWithEmails = handleNotificationWithEmails;