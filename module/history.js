"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/objectSpread"));

var _now = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/date/now"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _factory = _interopRequireDefault(require("../store/factories/factory"));

var _CustomerFactory = _interopRequireDefault(require("../database/factory/CustomerFactory"));

var _HistoryFactory = _interopRequireDefault(require("../database/factory/HistoryFactory"));

var _logger = _interopRequireDefault(require("./logger"));

var _default =
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(collection, object, code, attachment, context) {
    var user,
        objectWithUpdatedHistory,
        collectionFactory,
        customerFactory,
        historyFactory,
        customer,
        userId,
        addingHistory,
        history,
        objectToAddHistoryId,
        updatedObjectHistory,
        _args = arguments;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            user = _args.length > 5 && _args[5] !== undefined ? _args[5] : null;
            objectWithUpdatedHistory = object;
            _context.prev = 2;
            collectionFactory = new _factory.default();
            collectionFactory.collectionName = collection;
            collectionFactory.historyEnabled = false;
            customerFactory = new _CustomerFactory.default();
            historyFactory = new _HistoryFactory.default();
            customer = null, userId = null;

            if (!context) {
              _context.next = 15;
              break;
            }

            _context.next = 12;
            return customerFactory.getOne({
              context: context
            });

          case 12:
            customer = _context.sent;

            if (customer) {
              _context.next = 15;
              break;
            }

            throw new Error('Invalid customer');

          case 15:
            if (user) {
              userId = user;
            } else if (customer) {
              userId = customer.createdBy;
            }

            addingHistory = {
              code: code,
              attachment: attachment,
              createdAt: (0, _now.default)(),
              createdBy: userId
            };
            _context.next = 19;
            return historyFactory.create((0, _objectSpread2.default)({}, addingHistory), context);

          case 19:
            history = _context.sent;

            if (!objectWithUpdatedHistory._id) {
              _context.next = 31;
              break;
            }

            _context.next = 23;
            return collectionFactory.getOne({
              _id: object._id.toString()
            }, context);

          case 23:
            objectToAddHistoryId = _context.sent;
            updatedObjectHistory = objectToAddHistoryId.history || [];
            updatedObjectHistory.push(history._id.toString());
            _context.next = 28;
            return collectionFactory.update(object._id.toString(), {
              history: updatedObjectHistory
            }, context);

          case 28:
            objectWithUpdatedHistory = _context.sent;
            _context.next = 33;
            break;

          case 31:
            if (!objectWithUpdatedHistory.history) {
              objectWithUpdatedHistory.history = [];
            }

            objectWithUpdatedHistory.history.push(history._id.toString());

          case 33:
            _context.next = 38;
            break;

          case 35:
            _context.prev = 35;
            _context.t0 = _context["catch"](2);

            _logger.default.error("ADDING HISTORY for object by id ".concat(object._id, " by code ").concat(code, " with context ").concat(context, " and user ").concat(user, " *** ").concat(_context.t0.message));

          case 38:
            return _context.abrupt("return", objectWithUpdatedHistory);

          case 39:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[2, 35]]);
  }));

  return function (_x, _x2, _x3, _x4, _x5) {
    return _ref.apply(this, arguments);
  };
}();

exports.default = _default;