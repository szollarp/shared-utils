"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deleteMessageFromQueue = exports.watchQueueMessages = exports.sendMessageToQueue = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _rsmq = _interopRequireDefault(require("rsmq"));

var _rsmqWorker = _interopRequireDefault(require("rsmq-worker"));

var _logger = _interopRequireDefault(require("./logger"));

var _config = _interopRequireDefault(require("config"));

var _config$get = _config.default.get('queue.general'),
    host = _config$get.host,
    port = _config$get.port,
    ns = _config$get.ns;

var rsmq = new _rsmq.default({
  host: host,
  port: port,
  ns: ns
});

var createQueue = function createQueue(qname) {
  rsmq.createQueue({
    qname: qname
  }, function (err, resp) {
    if (resp === 1) {
      _logger.default.info("REDIS QUEUE *** Queue ".concat(qname, " created"));
    }
  });
};

var sendMessageToQueue =
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(qname, message) {
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return createQueue(qname);

          case 2:
            rsmq.sendMessage({
              qname: qname,
              message: message
            }, function (err, resp) {
              if (err) {
                _logger.default.error("REDIS QUEUE *** ".concat(qname, " *** ").concat(err.message));
              }

              if (resp) {
                _logger.default.info("REDIS QUEUE *** Message added to ".concat(qname, " queue: ").concat(message));
              }
            });

          case 3:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function sendMessageToQueue(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.sendMessageToQueue = sendMessageToQueue;

var watchQueueMessages = function watchQueueMessages(qname, callback) {
  var worker = new _rsmqWorker.default(qname, {
    workerConfig: _config.default.get('queue.worker'),
    rsmq: rsmq
  });
  worker.on("message",
  /*#__PURE__*/
  function () {
    var _ref2 = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee2(msg, next, id) {
      return _regenerator.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              callback(msg, id).then(function (res) {
                if (res.success || res.error) {
                  worker.del(id, function () {
                    _logger.default.info("REDIS QUEUE *** Message delete from ".concat(qname, ": ").concat(id));
                  });
                }
              }).catch(function (e) {
                _logger.default.error("ERROR ON REDIS QUEUE *** ".concat(qname));

                _logger.default.error(e.message);
              });

            case 1:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    return function (_x3, _x4, _x5) {
      return _ref2.apply(this, arguments);
    };
  }());
  worker.on('error', function (err, msg) {
    return _logger.default.error("REDIS QUEUE *** Queue ".concat(qname, " error on message: ").concat(msg.id, ", ").concat(err));
  });
  worker.on('exceeded', function (msg) {
    return _logger.default.warning("REDIS QUEUE *** Queue ".concat(qname, " exceeded on message: ").concat(msg.id));
  });
  return worker;
};

exports.watchQueueMessages = watchQueueMessages;

var deleteMessageFromQueue = function deleteMessageFromQueue(qname, id) {
  rsmq.deleteMessage({
    qname: qname,
    id: id
  }, function (err, resp) {
    if (err) {
      _logger.default.error("REDIS QUEUE *** ".concat(qname, " *** ").concat(err.message));
    }

    if (resp) {
      _logger.default.info("REDIS QUEUE *** Message removed from ".concat(qname, " by id: ").concat(id));
    }
  });
};

exports.deleteMessageFromQueue = deleteMessageFromQueue;