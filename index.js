"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _module2 = _interopRequireDefault(require("./module"));

var _database = _interopRequireDefault(require("./database"));

var _resource = _interopRequireDefault(require("./resource"));

var _default = {
  module: _module2.default,
  database: _database.default,
  resource: _resource.default
};
exports.default = _default;