"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _keys = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/object/keys"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/objectSpread"));

var _now = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/date/now"));

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _get2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/get"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _propertyValidator = require("property-validator");

var _config = _interopRequireDefault(require("config"));

var _axios = _interopRequireDefault(require("axios"));

var _randomstring = require("randomstring");

var _log = _interopRequireDefault(require("../../log"));

var _xml2js = _interopRequireDefault(require("xml2js"));

var _cryptoJs = _interopRequireDefault(require("crypto-js"));

var _index = require("../index");

var _utils = require("../../utils");

var _Factory = _interopRequireDefault(require("./Factory"));

var _UserFactory = _interopRequireDefault(require("./UserFactory"));

var _FilterFactory = _interopRequireDefault(require("./FilterFactory"));

var _InvitationFactory = _interopRequireDefault(require("./InvitationFactory"));

var _tokenExchange = _interopRequireDefault(require("../../module/xml-factory/token-exchange"));

var _constants = require("../../constants");

var _security = require("../../module/security");

var _notification = require("../../module/notification");

var CustomerFactory =
/*#__PURE__*/
function (_CollectionFacotry) {
  (0, _inherits2.default)(CustomerFactory, _CollectionFacotry);

  function CustomerFactory() {
    var _this;

    (0, _classCallCheck2.default)(this, CustomerFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(CustomerFactory).call(this));
    _this.collectionName = 'customers';
    _this.validationRules = {
      create: [(0, _propertyValidator.presence)('taxNumber'), (0, _propertyValidator.presence)('name')],
      update: [(0, _propertyValidator.presence)('taxNumber'), (0, _propertyValidator.presence)('name'), (0, _propertyValidator.presence)('auth')]
    };
    _this.userFactory = new _UserFactory.default();
    _this.filterFactory = new _FilterFactory.default();
    _this.invitationFactory = new _InvitationFactory.default();
    return _this;
  }

  (0, _createClass2.default)(CustomerFactory, [{
    key: "encryptCustomerAuth",
    value: function encryptCustomerAuth(auth) {
      if (!auth) return null;
      return {
        technicalUserName: auth.technicalUserName.trim(),
        technicalUserPassword: (0, _security.stringToBase64)(auth.technicalUserPassword.trim()),
        secretKey: (0, _security.stringToBase64)(auth.secretKey.trim()),
        exchangeKey: (0, _security.stringToBase64)(auth.exchangeKey.trim())
      };
    }
  }, {
    key: "readCustomerConfig",
    value: function () {
      var _readCustomerConfig = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(context) {
        var customer, user, taxNumber, _customer$auth, technicalUserName, technicalUserPassword, secretKey, exchangeKey;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return this.getOne({
                  context: context
                });

              case 2:
                customer = _context.sent;
                _context.next = 5;
                return this.userFactory.getOne({
                  _id: customer.createdBy
                });

              case 5:
                user = _context.sent;
                taxNumber = customer.taxNumber, _customer$auth = customer.auth, technicalUserName = _customer$auth.technicalUserName, technicalUserPassword = _customer$auth.technicalUserPassword, secretKey = _customer$auth.secretKey, exchangeKey = _customer$auth.exchangeKey;
                return _context.abrupt("return", {
                  login: technicalUserName,
                  password: (0, _security.base64ToString)(technicalUserPassword),
                  taxNumber: taxNumber,
                  signKey: (0, _security.base64ToString)(secretKey),
                  exchangeKey: (0, _security.base64ToString)(exchangeKey),
                  primaryEmail: user.email
                });

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function readCustomerConfig(_x) {
        return _readCustomerConfig.apply(this, arguments);
      }

      return readCustomerConfig;
    }()
  }, {
    key: "tokenExtract",
    value: function tokenExtract(taxNumber, tokenExchangeResponseXML, customerConfig) {
      var decodedExchangeTokenString = null;

      try {
        var tokenExchangeResponseXMLObject = null;

        _xml2js.default.parseString(tokenExchangeResponseXML, function (err, data) {
          if (err) {
            throw new Error(err);
          } else {
            tokenExchangeResponseXMLObject = data;
          }
        });

        var encodedExchangeToken = tokenExchangeResponseXMLObject.TokenExchangeResponse.encodedExchangeToken[0];

        var exchangeKey = _cryptoJs.default.enc.Utf8.parse(customerConfig.exchangeKey);

        var cipherParams = _cryptoJs.default.lib.CipherParams.create({
          ciphertext: _cryptoJs.default.enc.Base64.parse(encodedExchangeToken)
        });

        var decodedExchangeToken = _cryptoJs.default[_constants.TOKEN_ENCRYPTION.cipher].decrypt(cipherParams, exchangeKey, {
          mode: _cryptoJs.default.mode[_constants.TOKEN_ENCRYPTION.mode],
          padding: _cryptoJs.default.pad.NoPadding
        });

        decodedExchangeTokenString = _cryptoJs.default.enc.Utf8.stringify(decodedExchangeToken);
        decodedExchangeTokenString = decodedExchangeTokenString.replace(/[^a-zA-Z0-9-]/g, "");
      } catch (err) {
        _log.default.error("Error decrypting token for company with tax number ".concat(taxNumber, ": ").concat(err));
      }

      return decodedExchangeTokenString;
    }
  }, {
    key: "isValidCustomerAuth",
    value: function () {
      var _isValidCustomerAuth = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(taxNumber, login, password, signKey, exchangeKey) {
        var customerConfig, authErrorMessage, tokenExchangeXMLString, tokenExchangeEndpoint, tokenExchangeResponse, data, decryptedToken;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                customerConfig = {
                  login: login,
                  password: password,
                  taxNumber: taxNumber,
                  signKey: signKey,
                  exchangeKey: exchangeKey
                };
                authErrorMessage = null;

                _log.default.info("Validating customer with tax number ".concat(taxNumber));

                tokenExchangeXMLString = (0, _tokenExchange.default)(customerConfig);
                tokenExchangeEndpoint = (0, _utils.getProviderEndpoint)('TOKEN_EXCHANGE');
                _context2.next = 7;
                return _axios.default.post(tokenExchangeEndpoint, tokenExchangeXMLString, {
                  headers: {
                    'Content-Type': 'application/xml',
                    'Accept': 'application/xml'
                  }
                }).then(function (response) {
                  return response;
                }).catch(function (error) {
                  authErrorMessage = 'Invalid customer data. Please check your core tax number, technical username/password, secret key, or exchange key';
                  return error.response;
                });

              case 7:
                tokenExchangeResponse = _context2.sent;

                if (!tokenExchangeResponse || !tokenExchangeResponse.data.startsWith('<?xml')) {
                  authErrorMessage = 'HU TA service is temporarily unavailable, please, try again later';
                }

                if (!authErrorMessage) {
                  _context2.next = 11;
                  break;
                }

                return _context2.abrupt("return", authErrorMessage);

              case 11:
                data = tokenExchangeResponse.data;

                _log.default.info("Validating result of customer with tax number ".concat(taxNumber, ": ").concat(data));

                decryptedToken = this.tokenExtract(taxNumber, data, customerConfig);

                if (!decryptedToken) {
                  authErrorMessage = 'Could not decode token from HU TA, please, check your exchange key';
                }

                return _context2.abrupt("return", authErrorMessage || true);

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function isValidCustomerAuth(_x2, _x3, _x4, _x5, _x6) {
        return _isValidCustomerAuth.apply(this, arguments);
      }

      return isValidCustomerAuth;
    }()
  }, {
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5(args, user) {
        var _this2 = this;

        var name, taxNumber, seatPostcode, seatTown, seatAddress, seatCountry, auth, usersExistingCompanyWithTaxNumber, encryptedAuth, validationResult, currentUser, context, customerData, newCustomer, _config$get, supportEmail, sendWithRegistration, permissions, status, supportUser;

        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                name = args.name, taxNumber = args.taxNumber, seatPostcode = args.seatPostcode, seatTown = args.seatTown, seatAddress = args.seatAddress, seatCountry = args.seatCountry, auth = args.auth;

                if (!(!name || !taxNumber || !seatPostcode || !seatTown || !seatAddress || !seatCountry)) {
                  _context5.next = 5;
                  break;
                }

                throw new Error('Invalid data');

              case 5:
                if (!(auth && (!auth.technicalUserName || !auth.technicalUserPassword || !auth.exchangeKey || !auth.secretKey))) {
                  _context5.next = 7;
                  break;
                }

                throw new Error('Invalid data');

              case 7:
                _context5.prev = 7;
                _context5.next = 10;
                return this.getOne({
                  createdBy: user._id,
                  taxNumber: taxNumber
                });

              case 10:
                usersExistingCompanyWithTaxNumber = _context5.sent;

                if (!usersExistingCompanyWithTaxNumber) {
                  _context5.next = 13;
                  break;
                }

                throw new Error("Duplicated tax number. This Tax number has already been registrated at ".concat(usersExistingCompanyWithTaxNumber.name));

              case 13:
                encryptedAuth = {};

                if (!(auth && auth.technicalUserName && auth.technicalUserPassword && auth.exchangeKey && auth.secretKey)) {
                  _context5.next = 21;
                  break;
                }

                _context5.next = 17;
                return this.isValidCustomerAuth(taxNumber.trim(), auth.technicalUserName.trim(), auth.technicalUserPassword.trim(), auth.secretKey.trim(), auth.exchangeKey.trim());

              case 17:
                validationResult = _context5.sent;

                if (!(validationResult !== true)) {
                  _context5.next = 20;
                  break;
                }

                throw new Error(validationResult);

              case 20:
                encryptedAuth = this.encryptCustomerAuth(auth);

              case 21:
                _context5.next = 23;
                return this.userFactory.getOne({
                  _id: user._id
                });

              case 23:
                currentUser = _context5.sent;
                context = (0, _randomstring.generate)(15);
                customerData = {
                  createdAt: (0, _now.default)(),
                  createdBy: user._id,
                  context: context,
                  auth: encryptedAuth,
                  name: name,
                  taxNumber: taxNumber,
                  seatCountry: seatCountry,
                  seatPostcode: seatPostcode,
                  seatTown: seatTown,
                  seatAddress: seatAddress,
                  softwares: [],
                  notifications: _config.default.get('defaults.customer').notifications(currentUser.email),
                  features: _config.default.get('defaults.customer.features')
                };
                _context5.next = 28;
                return (0, _index.handleDbQuery)(
                /*#__PURE__*/
                (0, _asyncToGenerator2.default)(
                /*#__PURE__*/
                _regenerator.default.mark(function _callee3() {
                  var customerExists;
                  return _regenerator.default.wrap(function _callee3$(_context3) {
                    while (1) {
                      switch (_context3.prev = _context3.next) {
                        case 0:
                          customerExists = null;

                          if (customerExists) {
                            _context3.next = 7;
                            break;
                          }

                          _context3.next = 4;
                          return (0, _get2.default)((0, _getPrototypeOf2.default)(CustomerFactory.prototype), "create", _this2).call(_this2, customerData);

                        case 4:
                          return _context3.abrupt("return", _context3.sent);

                        case 7:
                          throw new Error('Missing or invalid customer data');

                        case 8:
                        case "end":
                          return _context3.stop();
                      }
                    }
                  }, _callee3, this);
                })));

              case 28:
                newCustomer = _context5.sent;

                if (_config.default.has('defaults.customer.filters')) {
                  _config.default.get('defaults.customer.filters').filters.map(
                  /*#__PURE__*/
                  function () {
                    var _ref2 = (0, _asyncToGenerator2.default)(
                    /*#__PURE__*/
                    _regenerator.default.mark(function _callee4(filter) {
                      return _regenerator.default.wrap(function _callee4$(_context4) {
                        while (1) {
                          switch (_context4.prev = _context4.next) {
                            case 0:
                              _context4.next = 2;
                              return _this2.filterFactory.create(filter, user, context);

                            case 2:
                            case "end":
                              return _context4.stop();
                          }
                        }
                      }, _callee4, this);
                    }));

                    return function (_x9) {
                      return _ref2.apply(this, arguments);
                    };
                  }());
                }

                _config$get = _config.default.get('defaults.customer.invitation'), supportEmail = _config$get.supportEmail, sendWithRegistration = _config$get.sendWithRegistration, permissions = _config$get.permissions, status = _config$get.status;

                if (!(supportEmail && sendWithRegistration && supportEmail !== currentUser.email)) {
                  _context5.next = 42;
                  break;
                }

                _context5.next = 34;
                return this.userFactory.getOne({
                  email: supportEmail
                });

              case 34:
                supportUser = _context5.sent;

                if (!supportUser) {
                  _context5.next = 42;
                  break;
                }

                _context5.next = 38;
                return this.invitationFactory.create({
                  email: supportEmail,
                  permissions: permissions,
                  status: status,
                  createdBy: currentUser._id,
                  userIsExisted: true
                }, context);

              case 38:
                _context5.next = 40;
                return this.userFactory.pushChildren(supportUser._id.toString(), {
                  permissions: (0, _objectSpread2.default)({
                    context: context
                  }, permissions, {
                    status: status
                  })
                });

              case 40:
                _context5.next = 42;
                return (0, _notification.handleNotificationWithEmails)('IRINA_INVITATION_SUCCESS', {
                  taxNumber: newCustomer.taxNumber,
                  name: newCustomer.name,
                  userName: currentUser.lastName + ' ' + currentUser.firstName
                }, [supportEmail]);

              case 42:
                return _context5.abrupt("return", newCustomer);

              case 45:
                _context5.prev = 45;
                _context5.t0 = _context5["catch"](7);
                throw new Error(_context5.t0);

              case 48:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[7, 45]]);
      }));

      function create(_x7, _x8) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }, {
    key: "update",
    value: function () {
      var _update = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee6(id, args, user) {
        var taxNumber, auth, encryptedAuth, customer, validationResult, customerData, _arr, _i, key;

        return _regenerator.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                taxNumber = args.taxNumber, auth = args.auth;
                encryptedAuth = {};
                _context6.prev = 2;
                _context6.next = 5;
                return (0, _get2.default)((0, _getPrototypeOf2.default)(CustomerFactory.prototype), "getOne", this).call(this, {
                  _id: id
                });

              case 5:
                customer = _context6.sent;

                if (customer) {
                  _context6.next = 8;
                  break;
                }

                throw new Error('Customer not exists');

              case 8:
                if (user._id.toString() !== customer.createdBy.toString()) {
                  taxNumber = customer.taxNumber;
                  args.taxNumber = taxNumber;
                }

                if (!(auth && auth.technicalUserName && auth.technicalUserPassword && auth.exchangeKey && auth.secretKey)) {
                  _context6.next = 16;
                  break;
                }

                _context6.next = 12;
                return this.isValidCustomerAuth(taxNumber.trim(), auth.technicalUserName.trim(), auth.technicalUserPassword.trim(), auth.secretKey.trim(), auth.exchangeKey.trim());

              case 12:
                validationResult = _context6.sent;

                if (!(validationResult !== true)) {
                  _context6.next = 15;
                  break;
                }

                throw new Error(validationResult);

              case 15:
                encryptedAuth = this.encryptCustomerAuth(auth);

              case 16:
                customerData = {};
                _arr = (0, _keys.default)(args);

                for (_i = 0; _i < _arr.length; _i++) {
                  key = _arr[_i];

                  if (key !== 'id' && (!args[key] && args[key] !== '' || (0, _keys.default)(args[key]))) {
                    customerData[key] = args[key];
                  }
                }

                if (encryptedAuth.technicalUserName) {
                  customerData.auth = encryptedAuth;
                }

                delete args.id;
                _context6.next = 23;
                return (0, _get2.default)((0, _getPrototypeOf2.default)(CustomerFactory.prototype), "update", this).call(this, id, (0, _objectSpread2.default)({}, customerData, {
                  updatedBy: user._id
                }));

              case 23:
                return _context6.abrupt("return", this.get({
                  createdBy: user._id
                }));

              case 26:
                _context6.prev = 26;
                _context6.t0 = _context6["catch"](2);
                throw new Error(_context6.t0);

              case 29:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[2, 26]]);
      }));

      function update(_x10, _x11, _x12) {
        return _update.apply(this, arguments);
      }

      return update;
    }()
  }]);
  return CustomerFactory;
}(_Factory.default);

var _default = CustomerFactory;
exports.default = _default;