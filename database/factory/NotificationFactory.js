"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _Factory = _interopRequireDefault(require("./Factory"));

var NotificationFactory =
/*#__PURE__*/
function (_CollectionFacotry) {
  (0, _inherits2.default)(NotificationFactory, _CollectionFacotry);

  function NotificationFactory() {
    var _this;

    (0, _classCallCheck2.default)(this, NotificationFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(NotificationFactory).call(this));
    _this.collectionName = 'notifications';
    _this.validationRules = {
      create: [],
      update: []
    };
    return _this;
  }

  return NotificationFactory;
}(_Factory.default);

var _default = NotificationFactory;
exports.default = _default;