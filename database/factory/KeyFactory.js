"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _Factory = _interopRequireDefault(require("./Factory"));

var KeyFactory =
/*#__PURE__*/
function (_CollectionFacotry) {
  (0, _inherits2.default)(KeyFactory, _CollectionFacotry);

  function KeyFactory() {
    var _this;

    (0, _classCallCheck2.default)(this, KeyFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(KeyFactory).call(this));
    _this.collectionName = 'keys';
    _this.validationRules = {
      create: [],
      update: []
    };
    return _this;
  }

  return KeyFactory;
}(_Factory.default);

var _default = KeyFactory;
exports.default = _default;