"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/objectSpread"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _get2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/get"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _xml2js = _interopRequireDefault(require("xml2js"));

var _config = _interopRequireDefault(require("config"));

var _constants = require("../../constants");

var _Factory = _interopRequireDefault(require("./Factory"));

var _InvoiceReportFactory = _interopRequireDefault(require("./InvoiceReportFactory"));

var _CustomerFactory = _interopRequireDefault(require("./CustomerFactory"));

var _notification = require("../../module/notification");

var IssueFactory =
/*#__PURE__*/
function (_CollectionFactory) {
  (0, _inherits2.default)(IssueFactory, _CollectionFactory);

  function IssueFactory(createdByModule) {
    var _this;

    (0, _classCallCheck2.default)(this, IssueFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(IssueFactory).call(this));
    _this.collectionName = 'issues';
    _this.invoiceReportFactory = new _InvoiceReportFactory.default();
    _this.customerFactory = new _CustomerFactory.default();
    _this.adminEmails = _config.default.get('email.mailer.toAddresses');
    _this.createdByModule = createdByModule || _constants.moduleNames.REPORTING;
    _this.validationRules = {
      create: [],
      update: []
    };
    return _this;
  }

  (0, _createClass2.default)(IssueFactory, [{
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(issuingObject, invoiceReportId, context) {
        var issue, response, returnIssue, xmlData, xmlDataObject, customer, existingToDoIssue, existingIssueWithThisInvoice, existingInvoice, invoiceReportForIssue, issueData;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (invoiceReportId) {
                  _context.next = 2;
                  break;
                }

                return _context.abrupt("return", null);

              case 2:
                issue = {
                  code: 'UNKNOWN'
                };
                response = issuingObject.response;
                returnIssue = null;
                _context.prev = 5;

                if (!(response && response.request && response.data && response.config)) {
                  _context.next = 15;
                  break;
                }

                xmlData = response.data;
                xmlDataObject = null;

                if (!xmlData.startsWith('<html>')) {
                  _context.next = 11;
                  break;
                }

                return _context.abrupt("return", returnIssue);

              case 11:
                _xml2js.default.parseString(xmlData, function (err, data) {
                  if (err) {
                    throw new Error(err);
                  }

                  xmlDataObject = data;
                });

                if (xmlDataObject.GeneralErrorResponse) {
                  issue.code = xmlDataObject.GeneralErrorResponse.result[0].errorCode[0];
                } else if (xmlDataObject.GeneralExceptionResponse) {
                  issue.code = xmlDataObject.GeneralExceptionResponse.errorCode[0];
                }

                _context.next = 16;
                break;

              case 15:
                issue.code = issuingObject.code;

              case 16:
                if (!_constants.IssueCodesToIgnore.includes(issue.code)) {
                  _context.next = 18;
                  break;
                }

                return _context.abrupt("return", null);

              case 18:
                issue.priority = _constants.IssueCodePriorities['HIGH'].includes(issue.code) ? 'HIGH' : _constants.IssueCodePriorities['LOW'].includes(issue.code) ? 'LOW' : null;

                if (!(!issue.priority && issue.code)) {
                  _context.next = 22;
                  break;
                }

                _context.next = 22;
                return (0, _notification.handleNotificationWithEmails)('UNREGISTERED_ISSUE_CODE', issue, this.adminEmails);

              case 22:
                if (!(issue.priority && issue.code)) {
                  _context.next = 52;
                  break;
                }

                issue.description = issue.code;
                _context.next = 26;
                return this.customerFactory.getOne({
                  context: context
                });

              case 26:
                customer = _context.sent;

                if (customer) {
                  _context.next = 29;
                  break;
                }

                throw new Error('Invalid customer');

              case 29:
                _context.next = 31;
                return this.getOne({
                  status: 'TODO',
                  code: issue.code
                }, context);

              case 31:
                existingToDoIssue = _context.sent;
                _context.next = 34;
                return this.getOne({
                  "code": issue.code,
                  "invoiceReports.id": invoiceReportId.toString()
                }, context);

              case 34:
                existingIssueWithThisInvoice = _context.sent;
                _context.next = 37;
                return this.invoiceReportFactory.getOne({
                  _id: invoiceReportId
                }, context);

              case 37:
                existingInvoice = _context.sent;

                if (existingInvoice) {
                  _context.next = 40;
                  break;
                }

                throw new Error("InvoiceReport with id '".concat(invoiceReportId, "' does not exists in customer's (").concat(context, ") DB"));

              case 40:
                invoiceReportForIssue = {
                  id: existingInvoice._id.toString(),
                  invoiceNumber: existingInvoice.invoiceNumber,
                  source: existingInvoice.source,
                  filename: existingInvoice.filename
                };

                if (existingIssueWithThisInvoice) {
                  _context.next = 52;
                  break;
                }

                if (!existingToDoIssue) {
                  _context.next = 48;
                  break;
                }

                _context.next = 45;
                return this.pushChildren(existingToDoIssue._id.toString(), {
                  'invoiceReports': invoiceReportForIssue
                }, context);

              case 45:
                returnIssue = _context.sent;
                _context.next = 52;
                break;

              case 48:
                issueData = (0, _objectSpread2.default)({}, issue, {
                  createdBy: customer.createdBy,
                  createdByModule: this.createdByModule,
                  status: 'TODO',
                  invoiceReports: [invoiceReportForIssue]
                });
                _context.next = 51;
                return (0, _get2.default)((0, _getPrototypeOf2.default)(IssueFactory.prototype), "create", this).call(this, issueData, context);

              case 51:
                returnIssue = _context.sent;

              case 52:
                if (!returnIssue) {
                  _context.next = 55;
                  break;
                }

                _context.next = 55;
                return (0, _notification.handleNotification)('ISSUE', returnIssue, context);

              case 55:
                _context.next = 60;
                break;

              case 57:
                _context.prev = 57;
                _context.t0 = _context["catch"](5);
                throw new Error(_context.t0);

              case 60:
                return _context.abrupt("return", returnIssue);

              case 61:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[5, 57]]);
      }));

      function create(_x, _x2, _x3) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }, {
    key: "createForFilesOnly",
    value: function () {
      var _createForFilesOnly = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(issuingObject, filename, context) {
        var issue, response, returnIssue, xmlData, xmlDataObject, customer, existingToDoIssue, existingIssueWithThisFile, fileForIssue, issueData;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (filename) {
                  _context2.next = 2;
                  break;
                }

                return _context2.abrupt("return", null);

              case 2:
                issue = {
                  code: 'UNKNOWN'
                };
                response = issuingObject.response;
                returnIssue = null;
                _context2.prev = 5;

                if (!(response && response.request && response.data && response.config)) {
                  _context2.next = 15;
                  break;
                }

                xmlData = response.data;
                xmlDataObject = null;

                if (!xmlData.startsWith('<html>')) {
                  _context2.next = 11;
                  break;
                }

                return _context2.abrupt("return", returnIssue);

              case 11:
                _xml2js.default.parseString(xmlData, function (err, data) {
                  if (err) {
                    throw new Error(err);
                  }

                  xmlDataObject = data;
                });

                if (xmlDataObject.GeneralErrorResponse) {
                  issue.code = xmlDataObject.GeneralErrorResponse.result[0].errorCode[0];
                } else if (xmlDataObject.GeneralExceptionResponse) {
                  issue.code = xmlDataObject.GeneralExceptionResponse.errorCode[0];
                }

                _context2.next = 16;
                break;

              case 15:
                issue.code = issuingObject.code;

              case 16:
                /* if (IssueCodesToIgnore.includes(issue.code)) {
                	return null
                } */
                issue.priority = _constants.IssueCodePriorities['HIGH'].includes(issue.code) ? 'HIGH' : _constants.IssueCodePriorities['LOW'].includes(issue.code) ? 'LOW' : null;

                if (!(!issue.priority && issue.code)) {
                  _context2.next = 20;
                  break;
                }

                _context2.next = 20;
                return (0, _notification.handleNotificationWithEmails)('UNREGISTERED_ISSUE_CODE', issue, this.adminEmails);

              case 20:
                if (!(issue.priority && issue.code)) {
                  _context2.next = 48;
                  break;
                }

                issue.description = issue.code;
                _context2.next = 24;
                return this.customerFactory.getOne({
                  context: context
                });

              case 24:
                customer = _context2.sent;

                if (customer) {
                  _context2.next = 27;
                  break;
                }

                throw new Error('Invalid customer');

              case 27:
                _context2.next = 29;
                return this.getOne({
                  status: 'TODO',
                  code: issue.code
                }, context);

              case 29:
                existingToDoIssue = _context2.sent;
                _context2.next = 32;
                return this.getOne({
                  "code": issue.code,
                  "files.filename": filename
                }, context);

              case 32:
                existingIssueWithThisFile = _context2.sent;
                fileForIssue = {
                  source: '',
                  filename: filename
                };

                if (existingIssueWithThisFile) {
                  _context2.next = 47;
                  break;
                }

                if (!existingToDoIssue) {
                  _context2.next = 41;
                  break;
                }

                _context2.next = 38;
                return this.pushChildren(existingToDoIssue._id.toString(), {
                  'files': fileForIssue
                }, context);

              case 38:
                returnIssue = _context2.sent;
                _context2.next = 45;
                break;

              case 41:
                issueData = (0, _objectSpread2.default)({}, issue, {
                  createdBy: customer.createdBy,
                  createdByModule: this.createdByModule,
                  status: 'TODO',
                  files: [fileForIssue]
                });
                _context2.next = 44;
                return (0, _get2.default)((0, _getPrototypeOf2.default)(IssueFactory.prototype), "create", this).call(this, issueData, context);

              case 44:
                returnIssue = _context2.sent;

              case 45:
                _context2.next = 48;
                break;

              case 47:
                returnIssue = existingIssueWithThisFile;

              case 48:
                if (!returnIssue) {
                  _context2.next = 51;
                  break;
                }

                _context2.next = 51;
                return (0, _notification.handleNotification)('ISSUE', returnIssue, context);

              case 51:
                _context2.next = 56;
                break;

              case 53:
                _context2.prev = 53;
                _context2.t0 = _context2["catch"](5);
                throw new Error(_context2.t0);

              case 56:
                return _context2.abrupt("return", returnIssue);

              case 57:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[5, 53]]);
      }));

      function createForFilesOnly(_x4, _x5, _x6) {
        return _createForFilesOnly.apply(this, arguments);
      }

      return createForFilesOnly;
    }()
  }, {
    key: "createForInvoicesOnly",
    value: function () {
      var _createForInvoicesOnly = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(issuingObject, invoiceNumber, context) {
        var source,
            issue,
            returnIssue,
            customer,
            invoiceForIssue,
            existingToDoIssue,
            existingIssueWithThisInvoice,
            issueData,
            _args3 = arguments;
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                source = _args3.length > 3 && _args3[3] !== undefined ? _args3[3] : '';

                if (invoiceNumber) {
                  _context3.next = 3;
                  break;
                }

                return _context3.abrupt("return", null);

              case 3:
                issue = {
                  code: 'UNKNOWN'
                };
                returnIssue = null;
                _context3.prev = 5;
                issue.code = issuingObject.code;

                if (!_constants.IssueCodesToIgnore.includes(issue.code)) {
                  _context3.next = 9;
                  break;
                }

                return _context3.abrupt("return", null);

              case 9:
                issue.priority = _constants.IssueCodePriorities['HIGH'].includes(issue.code) ? 'HIGH' : _constants.IssueCodePriorities['LOW'].includes(issue.code) ? 'LOW' : null;

                if (!(!issue.priority && issue.code)) {
                  _context3.next = 13;
                  break;
                }

                _context3.next = 13;
                return (0, _notification.handleNotificationWithEmails)('UNREGISTERED_ISSUE_CODE', issue, this.adminEmails);

              case 13:
                if (!(issue.priority && issue.code)) {
                  _context3.next = 38;
                  break;
                }

                issue.description = issue.code;
                _context3.next = 17;
                return this.customerFactory.getOne({
                  context: context
                });

              case 17:
                customer = _context3.sent;

                if (customer) {
                  _context3.next = 20;
                  break;
                }

                throw new Error('Invalid customer');

              case 20:
                invoiceForIssue = {
                  source: source,
                  invoiceNumber: invoiceNumber
                };
                _context3.next = 23;
                return this.getOne({
                  status: 'TODO',
                  code: issue.code
                }, context);

              case 23:
                existingToDoIssue = _context3.sent;
                _context3.next = 26;
                return this.getOne({
                  "code": issue.code,
                  invoices: invoiceForIssue
                }, context);

              case 26:
                existingIssueWithThisInvoice = _context3.sent;

                if (existingIssueWithThisInvoice) {
                  _context3.next = 38;
                  break;
                }

                if (!existingToDoIssue) {
                  _context3.next = 34;
                  break;
                }

                _context3.next = 31;
                return this.pushChildren(existingToDoIssue._id.toString(), {
                  'invoices': invoiceForIssue
                }, context);

              case 31:
                returnIssue = _context3.sent;
                _context3.next = 38;
                break;

              case 34:
                issueData = (0, _objectSpread2.default)({}, issue, {
                  createdBy: customer.createdBy,
                  createdByModule: this.createdByModule,
                  status: 'TODO',
                  invoices: [invoiceForIssue]
                });
                _context3.next = 37;
                return (0, _get2.default)((0, _getPrototypeOf2.default)(IssueFactory.prototype), "create", this).call(this, issueData, context);

              case 37:
                returnIssue = _context3.sent;

              case 38:
                if (!returnIssue) {
                  _context3.next = 41;
                  break;
                }

                _context3.next = 41;
                return (0, _notification.handleNotification)('ISSUE', returnIssue, context);

              case 41:
                _context3.next = 46;
                break;

              case 43:
                _context3.prev = 43;
                _context3.t0 = _context3["catch"](5);
                throw new Error(_context3.t0);

              case 46:
                return _context3.abrupt("return", returnIssue);

              case 47:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[5, 43]]);
      }));

      function createForInvoicesOnly(_x7, _x8, _x9) {
        return _createForInvoicesOnly.apply(this, arguments);
      }

      return createForInvoicesOnly;
    }()
  }]);
  return IssueFactory;
}(_Factory.default);

var _default = IssueFactory;
exports.default = _default;