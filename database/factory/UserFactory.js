"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _now = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/date/now"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/objectSpread"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _get2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/get"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _bcrypt = _interopRequireDefault(require("bcrypt"));

var _owaspPasswordStrengthTest = _interopRequireDefault(require("owasp-password-strength-test"));

var _config = _interopRequireDefault(require("config"));

var _propertyValidator = require("property-validator");

var _handler = require("../handler");

var _Factory = _interopRequireDefault(require("./Factory"));

var _InvitationFactory = _interopRequireDefault(require("./InvitationFactory"));

var USER_TYPE_ACTIVE = 'ACTIVE';

var UserFactory =
/*#__PURE__*/
function (_CollectionFacotry) {
  (0, _inherits2.default)(UserFactory, _CollectionFacotry);

  function UserFactory() {
    var _this;

    (0, _classCallCheck2.default)(this, UserFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(UserFactory).call(this));
    _this.collectionName = 'users';
    _this.validationRules = {
      create: [(0, _propertyValidator.presence)('firstName'), (0, _propertyValidator.presence)('lastName'), (0, _propertyValidator.presence)('password'), (0, _propertyValidator.email)('email')],
      update: [(0, _propertyValidator.presence)('firstName'), (0, _propertyValidator.presence)('lastName'), (0, _propertyValidator.presence)('password'), (0, _propertyValidator.email)('email')]
    };
    _this.invitationFactory = new _InvitationFactory.default();
    return _this;
  }

  (0, _createClass2.default)(UserFactory, [{
    key: "validatePassword",
    value: function validatePassword(password, passwordVerify) {
      if (password !== passwordVerify) return false;

      var pswTest = _owaspPasswordStrengthTest.default.test(password);

      if (pswTest.errors.length > 0) return false;
      return true;
    }
  }, {
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(args) {
        var _this2 = this;

        var invitation, context, _config$get, asInvite, asRegistration, userData;

        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (this.validatePassword(args.password, args.passwordVerify)) {
                  _context2.next = 2;
                  break;
                }

                throw new Error('Missing or invalid registration data');

              case 2:
                invitation = null;
                context = null;

                if (!(args.token && args.context)) {
                  _context2.next = 9;
                  break;
                }

                _context2.next = 7;
                return this.invitationFactory.getOne({
                  token: args.token
                }, args.context);

              case 7:
                invitation = _context2.sent;
                context = args.context;

              case 9:
                delete args.passwordVerify;
                delete args.token;
                delete args.context;
                _config$get = _config.default.get('defaults.licences'), asInvite = _config$get.asInvite, asRegistration = _config$get.asRegistration;
                _context2.t0 = _objectSpread2.default;
                _context2.t1 = {};
                _context2.t2 = args;
                _context2.next = 18;
                return _bcrypt.default.hash(args.password, 12);

              case 18:
                _context2.t3 = _context2.sent;
                _context2.t4 = USER_TYPE_ACTIVE;
                _context2.t5 = (0, _now.default)();
                _context2.t6 = {
                  softwarePerCustomer: asInvite || asRegistration
                };
                _context2.t7 = {
                  password: _context2.t3,
                  status: _context2.t4,
                  createdAt: _context2.t5,
                  config: _context2.t6
                };
                userData = (0, _context2.t0)(_context2.t1, _context2.t2, _context2.t7);
                _context2.prev = 24;
                (0, _propertyValidator.validate)(userData, this.validationRules.create);
                _context2.next = 28;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee(db) {
                    var userExists, newUser;
                    return _regenerator.default.wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            _context.next = 2;
                            return db.collection(_this2.collectionName).findOne({
                              email: userData.email
                            });

                          case 2:
                            userExists = _context.sent;

                            if (userExists) {
                              _context.next = 15;
                              break;
                            }

                            _context.next = 6;
                            return (0, _get2.default)((0, _getPrototypeOf2.default)(UserFactory.prototype), "create", _this2).call(_this2, (0, _objectSpread2.default)({}, userData));

                          case 6:
                            newUser = _context.sent;

                            if (!invitation) {
                              _context.next = 12;
                              break;
                            }

                            _context.next = 10;
                            return _this2.pushChildren(newUser._id.toString(), {
                              permissions: (0, _objectSpread2.default)({
                                context: context
                              }, invitation.permissions, {
                                status: 'ACTIVE'
                              })
                            });

                          case 10:
                            _context.next = 12;
                            return _this2.invitationFactory.update(invitation._id.toString(), {
                              status: 'ACTIVE'
                            }, context);

                          case 12:
                            return _context.abrupt("return", newUser);

                          case 15:
                            throw new Error('Missing or invalid registration data');

                          case 16:
                          case "end":
                            return _context.stop();
                        }
                      }
                    }, _callee, this);
                  }));

                  return function (_x2) {
                    return _ref.apply(this, arguments);
                  };
                }());

              case 28:
                return _context2.abrupt("return", _context2.sent);

              case 31:
                _context2.prev = 31;
                _context2.t8 = _context2["catch"](24);
                throw new Error(_context2.t8);

              case 34:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[24, 31]]);
      }));

      function create(_x) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }]);
  return UserFactory;
}(_Factory.default);

var _default = UserFactory;
exports.default = _default;