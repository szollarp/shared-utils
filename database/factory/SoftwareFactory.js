"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _now = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/date/now"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/objectSpread"));

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/slicedToArray"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _get2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/get"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _propertyValidator = require("property-validator");

var _path = _interopRequireDefault(require("path"));

var _fsMagic = _interopRequireDefault(require("fs-magic"));

var _config = _interopRequireDefault(require("config"));

var _handler = require("../handler");

var _Factory = _interopRequireDefault(require("./Factory"));

var _CustomerFactory = _interopRequireDefault(require("./CustomerFactory"));

var _UserFactory = _interopRequireDefault(require("./UserFactory"));

var _FtpConfigFactory = _interopRequireDefault(require("./FtpConfigFactory"));

var SoftwareFactory =
/*#__PURE__*/
function (_CollectionFactory) {
  (0, _inherits2.default)(SoftwareFactory, _CollectionFactory);

  function SoftwareFactory() {
    var _this;

    (0, _classCallCheck2.default)(this, SoftwareFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(SoftwareFactory).call(this));
    _this.collectionName = 'softwares';
    _this.customerFactory = new _CustomerFactory.default();
    _this.userFactory = new _UserFactory.default();
    _this.ftpConfigFactory = new _FtpConfigFactory.default();
    _this.validationRules = {
      create: [(0, _propertyValidator.presence)('softwareId'), (0, _propertyValidator.presence)('softwareName')],
      update: [(0, _propertyValidator.presence)('softwareId'), (0, _propertyValidator.presence)('softwareName')]
    };
    return _this;
  }

  (0, _createClass2.default)(SoftwareFactory, [{
    key: "isSoftwareRemovable",
    value: function () {
      var _isSoftwareRemovable = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(dirname) {
        var _ref, _ref2, files, uploads;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _fsMagic.default.scandir(dirname, true, true);

              case 2:
                _ref = _context.sent;
                _ref2 = (0, _slicedToArray2.default)(_ref, 1);
                files = _ref2[0];
                uploads = files.filter(function (name) {
                  return name.includes('/uploads/');
                });

                if (!uploads.length) {
                  _context.next = 8;
                  break;
                }

                return _context.abrupt("return", false);

              case 8:
                return _context.abrupt("return", true);

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function isSoftwareRemovable(_x) {
        return _isSoftwareRemovable.apply(this, arguments);
      }

      return isSoftwareRemovable;
    }()
  }, {
    key: "remove",
    value: function () {
      var _remove = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(_id, user) {
        var context,
            ftpConfig,
            dirname,
            isSoftwareRemovable,
            customer,
            _args2 = arguments;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                context = _args2.length > 2 && _args2[2] !== undefined ? _args2[2] : null;
                _context2.next = 3;
                return this.ftpConfigFactory.getOne({
                  softwareId: _id
                }, context);

              case 3:
                ftpConfig = _context2.sent;

                if (!ftpConfig) {
                  _context2.next = 13;
                  break;
                }

                dirname = _path.default.join(_config.default.get('service.classifier.directory'), "".concat(context).concat(ftpConfig.username));
                _context2.next = 8;
                return this.isSoftwareRemovable(dirname);

              case 8:
                isSoftwareRemovable = _context2.sent;

                if (isSoftwareRemovable) {
                  _context2.next = 11;
                  break;
                }

                throw new Error('The directory is not empty');

              case 11:
                _context2.next = 13;
                return this.ftpConfigFactory.remove(ftpConfig._id.toString(), context, true);

              case 13:
                _context2.next = 15;
                return (0, _get2.default)((0, _getPrototypeOf2.default)(SoftwareFactory.prototype), "remove", this).call(this, _id.toString(), context, true);

              case 15:
                _context2.next = 17;
                return this.customerFactory.getOne({
                  context: context
                });

              case 17:
                customer = _context2.sent;
                _context2.next = 20;
                return this.customerFactory.removeChildren(customer._id.toString(), {
                  "softwares": {
                    "$in": [_id.toString()]
                  },
                  "updatedBy": user._id
                });

              case 20:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function remove(_x2, _x3) {
        return _remove.apply(this, arguments);
      }

      return remove;
    }()
  }, {
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(args, user, context) {
        var _this2 = this;

        var softwareId, softwareName, customer, softwareData, currentUser;
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                softwareId = args.softwareId, softwareName = args.softwareName;

                if (!(!context || !softwareId || !softwareName)) {
                  _context4.next = 3;
                  break;
                }

                throw new Error('Invalid data');

              case 3:
                _context4.prev = 3;
                _context4.next = 6;
                return this.customerFactory.getOne({
                  context: context
                });

              case 6:
                customer = _context4.sent;
                softwareData = (0, _objectSpread2.default)({
                  createdAt: (0, _now.default)(),
                  createdBy: user._id
                }, args);
                _context4.next = 10;
                return this.userFactory.getOne({
                  _id: user._id
                });

              case 10:
                currentUser = _context4.sent;
                _context4.next = 13;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref3 = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee3(db) {
                    var softwareExists, newSoftware;
                    return _regenerator.default.wrap(function _callee3$(_context3) {
                      while (1) {
                        switch (_context3.prev = _context3.next) {
                          case 0:
                            _context3.next = 2;
                            return db.collection(_this2.collectionName).findOne({
                              softwareId: softwareId,
                              softwareName: softwareName,
                              deletedAt: {
                                $exists: false
                              }
                            });

                          case 2:
                            softwareExists = _context3.sent;

                            if (!softwareExists) {
                              _context3.next = 5;
                              break;
                            }

                            throw new Error('Missing or invalid software data');

                          case 5:
                            if (!(currentUser.config.softwarePerCustomer < customer.softwares.length + 1)) {
                              _context3.next = 7;
                              break;
                            }

                            throw new Error('To much software');

                          case 7:
                            _context3.next = 9;
                            return (0, _get2.default)((0, _getPrototypeOf2.default)(SoftwareFactory.prototype), "create", _this2).call(_this2, softwareData, context);

                          case 9:
                            newSoftware = _context3.sent;
                            _context3.next = 12;
                            return _this2.customerFactory.pushChildren(customer._id.toString(), {
                              'softwares': newSoftware._id.toString(),
                              'updatedBy': user._id
                            });

                          case 12:
                            return _context3.abrupt("return", newSoftware);

                          case 13:
                          case "end":
                            return _context3.stop();
                        }
                      }
                    }, _callee3, this);
                  }));

                  return function (_x7) {
                    return _ref3.apply(this, arguments);
                  };
                }(), context);

              case 13:
                return _context4.abrupt("return", _context4.sent);

              case 16:
                _context4.prev = 16;
                _context4.t0 = _context4["catch"](3);
                throw new Error(_context4.t0);

              case 19:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[3, 16]]);
      }));

      function create(_x4, _x5, _x6) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }]);
  return SoftwareFactory;
}(_Factory.default);

var _default = SoftwareFactory;
exports.default = _default;