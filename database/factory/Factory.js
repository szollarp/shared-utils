"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _stringify = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/json/stringify"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/typeof"));

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _isArray = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/array/is-array"));

var _now = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/date/now"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/objectSpread"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _keys = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/object/keys"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/defineProperty"));

var _mongodb = require("mongodb");

var _history = _interopRequireDefault(require("../../module/history"));

var _constants = require("../../constants");

var _handler = require("../handler");

var CollectionFactory =
/*#__PURE__*/
function () {
  function CollectionFactory() {
    var _this = this;

    (0, _classCallCheck2.default)(this, CollectionFactory);
    (0, _defineProperty2.default)(this, "isHistoryEnabled", function () {
      return _this.historyEnabled && _this.collectionName !== 'invoicereports';
    });
    this.collectionName = null;
    this.historyEnabled = true;
    this.validationRules = {
      create: [],
      update: []
    };
  }

  (0, _createClass2.default)(CollectionFactory, [{
    key: "isOnlyHistoryAdded",
    value: function isOnlyHistoryAdded(query) {
      var keys = (0, _keys.default)(query);
      var key = keys[0];
      return keys.length === 1 && key === "history";
    }
  }, {
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(query) {
        var _this2 = this;

        var context,
            _args2 = arguments;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                context = _args2.length > 1 && _args2[1] !== undefined ? _args2[1] : null;

                if (query) {
                  _context2.next = 3;
                  break;
                }

                return _context2.abrupt("return", null);

              case 3:
                _context2.prev = 3;
                _context2.next = 6;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee(db) {
                    var newObject, objectCreated;
                    return _regenerator.default.wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            _context.next = 2;
                            return db.collection(_this2.collectionName).insertOne((0, _objectSpread2.default)({}, query, {
                              createdAt: (0, _now.default)()
                            }));

                          case 2:
                            newObject = _context.sent;
                            objectCreated = (0, _isArray.default)(newObject.ops) ? newObject.ops.pop() : newObject.ops;

                            if (!_this2.isHistoryEnabled()) {
                              _context.next = 7;
                              break;
                            }

                            _context.next = 7;
                            return (0, _history.default)(_this2.collectionName, objectCreated, _constants.HistoryCode.ELEMENT_CREATED, query, context, query.createdBy);

                          case 7:
                            ;
                            return _context.abrupt("return", objectCreated);

                          case 9:
                          case "end":
                            return _context.stop();
                        }
                      }
                    }, _callee, this);
                  }));

                  return function (_x2) {
                    return _ref.apply(this, arguments);
                  };
                }(), context);

              case 6:
                return _context2.abrupt("return", _context2.sent);

              case 9:
                _context2.prev = 9;
                _context2.t0 = _context2["catch"](3);
                throw new Error(_context2.t0);

              case 12:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[3, 9]]);
      }));

      function create(_x) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }, {
    key: "update",
    value: function () {
      var _update = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(_id, query) {
        var _this3 = this;

        var context,
            updatedAt,
            setObject,
            _args4 = arguments;
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                context = _args4.length > 2 && _args4[2] !== undefined ? _args4[2] : null;

                if (_id) {
                  _context4.next = 3;
                  break;
                }

                return _context4.abrupt("return", null);

              case 3:
                _id = _mongodb.ObjectID.createFromHexString(_id);
                updatedAt = this.isOnlyHistoryAdded(query) ? null : (0, _now.default)();
                setObject = (0, _objectSpread2.default)({}, query);

                if (updatedAt) {
                  setObject.updatedAt = updatedAt;
                }

                ;
                delete setObject.id;
                delete setObject._id;
                _context4.prev = 10;
                _context4.next = 13;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref2 = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee3(db) {
                    var response, updatedObject;
                    return _regenerator.default.wrap(function _callee3$(_context3) {
                      while (1) {
                        switch (_context3.prev = _context3.next) {
                          case 0:
                            _context3.next = 2;
                            return db.collection(_this3.collectionName).findOneAndUpdate({
                              _id: _id
                            }, {
                              $set: setObject
                            }, {
                              returnOriginal: false
                            });

                          case 2:
                            response = _context3.sent;

                            if (!response.ok) {
                              _context3.next = 11;
                              break;
                            }

                            updatedObject = response.value;

                            if (!_this3.isHistoryEnabled()) {
                              _context3.next = 8;
                              break;
                            }

                            _context3.next = 8;
                            return (0, _history.default)(_this3.collectionName, updatedObject, _constants.HistoryCode.ELEMENT_UPDATED, query, context, query.updatedBy);

                          case 8:
                            return _context3.abrupt("return", updatedObject);

                          case 11:
                            throw new Error('Error during update');

                          case 12:
                            ;

                          case 13:
                          case "end":
                            return _context3.stop();
                        }
                      }
                    }, _callee3, this);
                  }));

                  return function (_x5) {
                    return _ref2.apply(this, arguments);
                  };
                }(), context);

              case 13:
                return _context4.abrupt("return", _context4.sent);

              case 16:
                _context4.prev = 16;
                _context4.t0 = _context4["catch"](10);
                throw new Error(_context4.t0);

              case 19:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[10, 16]]);
      }));

      function update(_x3, _x4) {
        return _update.apply(this, arguments);
      }

      return update;
    }()
  }, {
    key: "get",
    value: function () {
      var _get = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee6(query) {
        var _this4 = this;

        var context,
            operations,
            orderBy,
            _args6 = arguments;
        return _regenerator.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                context = _args6.length > 1 && _args6[1] !== undefined ? _args6[1] : null;
                operations = _args6.length > 2 && _args6[2] !== undefined ? _args6[2] : {};
                orderBy = _args6.length > 3 && _args6[3] !== undefined ? _args6[3] : {};

                if (query._id && (0, _typeof2.default)(query._id) !== _mongodb.ObjectID) {
                  query._id = _mongodb.ObjectID.createFromHexString(query._id);
                }

                _context6.prev = 4;
                _context6.next = 7;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref3 = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee5(db) {
                    var results;
                    return _regenerator.default.wrap(function _callee5$(_context5) {
                      while (1) {
                        switch (_context5.prev = _context5.next) {
                          case 0:
                            _context5.next = 2;
                            return db.collection(_this4.collectionName).find(query);

                          case 2:
                            results = _context5.sent;

                            if (!orderBy) {
                              _context5.next = 7;
                              break;
                            }

                            _context5.next = 6;
                            return results.sort(orderBy);

                          case 6:
                            results = _context5.sent;

                          case 7:
                            if (!operations.skip) {
                              _context5.next = 11;
                              break;
                            }

                            _context5.next = 10;
                            return results.skip(operations.skip);

                          case 10:
                            results = _context5.sent;

                          case 11:
                            if (!operations.limit) {
                              _context5.next = 15;
                              break;
                            }

                            _context5.next = 14;
                            return results.limit(operations.limit);

                          case 14:
                            results = _context5.sent;

                          case 15:
                            return _context5.abrupt("return", results.toArray());

                          case 16:
                          case "end":
                            return _context5.stop();
                        }
                      }
                    }, _callee5, this);
                  }));

                  return function (_x7) {
                    return _ref3.apply(this, arguments);
                  };
                }(), context);

              case 7:
                return _context6.abrupt("return", _context6.sent);

              case 10:
                _context6.prev = 10;
                _context6.t0 = _context6["catch"](4);
                throw new Error(_context6.t0);

              case 13:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[4, 10]]);
      }));

      function get(_x6) {
        return _get.apply(this, arguments);
      }

      return get;
    }()
  }, {
    key: "getByIds",
    value: function () {
      var _getByIds = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee8(idsArray) {
        var _this5 = this;

        var context,
            orderBy,
            _args8 = arguments;
        return _regenerator.default.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                context = _args8.length > 1 && _args8[1] !== undefined ? _args8[1] : null;
                orderBy = _args8.length > 2 && _args8[2] !== undefined ? _args8[2] : {};
                idsArray = idsArray.map(function (id) {
                  if ((0, _typeof2.default)(id) !== _mongodb.ObjectID) {
                    id = _mongodb.ObjectID.createFromHexString(id);
                  }

                  return id;
                });
                _context8.prev = 3;
                _context8.next = 6;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref4 = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee7(db) {
                    var objects;
                    return _regenerator.default.wrap(function _callee7$(_context7) {
                      while (1) {
                        switch (_context7.prev = _context7.next) {
                          case 0:
                            _context7.next = 2;
                            return db.collection(_this5.collectionName).find({
                              _id: {
                                $in: idsArray
                              }
                            });

                          case 2:
                            objects = _context7.sent;

                            if (!orderBy) {
                              _context7.next = 7;
                              break;
                            }

                            _context7.next = 6;
                            return objects.sort(orderBy);

                          case 6:
                            objects = _context7.sent;

                          case 7:
                            return _context7.abrupt("return", objects.toArray());

                          case 8:
                          case "end":
                            return _context7.stop();
                        }
                      }
                    }, _callee7, this);
                  }));

                  return function (_x9) {
                    return _ref4.apply(this, arguments);
                  };
                }(), context);

              case 6:
                return _context8.abrupt("return", _context8.sent);

              case 9:
                _context8.prev = 9;
                _context8.t0 = _context8["catch"](3);
                throw new Error(_context8.t0);

              case 12:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this, [[3, 9]]);
      }));

      function getByIds(_x8) {
        return _getByIds.apply(this, arguments);
      }

      return getByIds;
    }()
  }, {
    key: "getLimited",
    value: function () {
      var _getLimited = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee10(query, limit) {
        var _this6 = this;

        var context,
            _args10 = arguments;
        return _regenerator.default.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                context = _args10.length > 2 && _args10[2] !== undefined ? _args10[2] : null;

                if (query._id && (0, _typeof2.default)(query._id) !== _mongodb.ObjectID) {
                  query._id = _mongodb.ObjectID.createFromHexString(query._id);
                }

                _context10.prev = 2;
                _context10.next = 5;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref5 = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee9(db) {
                    return _regenerator.default.wrap(function _callee9$(_context9) {
                      while (1) {
                        switch (_context9.prev = _context9.next) {
                          case 0:
                            _context9.next = 2;
                            return db.collection(_this6.collectionName).find(query).limit(limit).toArray();

                          case 2:
                            return _context9.abrupt("return", _context9.sent);

                          case 3:
                          case "end":
                            return _context9.stop();
                        }
                      }
                    }, _callee9, this);
                  }));

                  return function (_x12) {
                    return _ref5.apply(this, arguments);
                  };
                }(), context);

              case 5:
                return _context10.abrupt("return", _context10.sent);

              case 8:
                _context10.prev = 8;
                _context10.t0 = _context10["catch"](2);
                throw new Error(_context10.t0);

              case 11:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10, this, [[2, 8]]);
      }));

      function getLimited(_x10, _x11) {
        return _getLimited.apply(this, arguments);
      }

      return getLimited;
    }()
  }, {
    key: "getOneWithFields",
    value: function () {
      var _getOneWithFields = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee12(query, fields) {
        var _this7 = this;

        var context,
            response,
            _args12 = arguments;
        return _regenerator.default.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                context = _args12.length > 2 && _args12[2] !== undefined ? _args12[2] : null;

                if (query._id && (0, _typeof2.default)(query._id) !== _mongodb.ObjectID) {
                  query._id = _mongodb.ObjectID.createFromHexString(query._id);
                }

                _context12.prev = 2;
                _context12.next = 5;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref6 = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee11(db) {
                    return _regenerator.default.wrap(function _callee11$(_context11) {
                      while (1) {
                        switch (_context11.prev = _context11.next) {
                          case 0:
                            _context11.next = 2;
                            return db.collection(_this7.collectionName).find(query, fields).toArray();

                          case 2:
                            return _context11.abrupt("return", _context11.sent);

                          case 3:
                          case "end":
                            return _context11.stop();
                        }
                      }
                    }, _callee11, this);
                  }));

                  return function (_x15) {
                    return _ref6.apply(this, arguments);
                  };
                }(), context);

              case 5:
                response = _context12.sent;
                return _context12.abrupt("return", response.pop());

              case 9:
                _context12.prev = 9;
                _context12.t0 = _context12["catch"](2);
                throw new Error(_context12.t0);

              case 12:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12, this, [[2, 9]]);
      }));

      function getOneWithFields(_x13, _x14) {
        return _getOneWithFields.apply(this, arguments);
      }

      return getOneWithFields;
    }()
  }, {
    key: "getOne",
    value: function () {
      var _getOne = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee13(query) {
        var context,
            response,
            _args13 = arguments;
        return _regenerator.default.wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                context = _args13.length > 1 && _args13[1] !== undefined ? _args13[1] : null;
                _context13.next = 3;
                return this.get(query, context);

              case 3:
                response = _context13.sent;
                return _context13.abrupt("return", response.pop());

              case 5:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13, this);
      }));

      function getOne(_x16) {
        return _getOne.apply(this, arguments);
      }

      return getOne;
    }()
  }, {
    key: "removeChildren",
    value: function () {
      var _removeChildren = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee15(_id, pull, context) {
        var _this8 = this;

        var userId;
        return _regenerator.default.wrap(function _callee15$(_context15) {
          while (1) {
            switch (_context15.prev = _context15.next) {
              case 0:
                if (_id) {
                  _context15.next = 2;
                  break;
                }

                return _context15.abrupt("return", pull);

              case 2:
                _id = _mongodb.ObjectID.createFromHexString(_id);
                userId = pull.createdBy || pull.updatedBy || null;
                delete pull['updatedBy'];
                delete pull['createdBy'];
                _context15.prev = 6;
                _context15.next = 9;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref7 = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee14(db) {
                    var response, value;
                    return _regenerator.default.wrap(function _callee14$(_context14) {
                      while (1) {
                        switch (_context14.prev = _context14.next) {
                          case 0:
                            _context14.next = 2;
                            return db.collection(_this8.collectionName).findOneAndUpdate({
                              _id: _id
                            }, {
                              $pull: (0, _objectSpread2.default)({}, pull)
                            }, {
                              returnOriginal: false,
                              multi: true
                            });

                          case 2:
                            response = _context14.sent;

                            if (!response.ok) {
                              _context14.next = 11;
                              break;
                            }

                            value = response.value;

                            if (!_this8.isHistoryEnabled()) {
                              _context14.next = 8;
                              break;
                            }

                            _context14.next = 8;
                            return (0, _history.default)(_this8.collectionName, value, _constants.HistoryCode.ELEMENT_CHILDREN_PULLED, (0, _stringify.default)(pull), context, userId);

                          case 8:
                            return _context14.abrupt("return", value);

                          case 11:
                            throw new Error('Error during update');

                          case 12:
                          case "end":
                            return _context14.stop();
                        }
                      }
                    }, _callee14, this);
                  }));

                  return function (_x20) {
                    return _ref7.apply(this, arguments);
                  };
                }(), context);

              case 9:
                return _context15.abrupt("return", _context15.sent);

              case 12:
                _context15.prev = 12;
                _context15.t0 = _context15["catch"](6);
                throw new Error(_context15.t0);

              case 15:
              case "end":
                return _context15.stop();
            }
          }
        }, _callee15, this, [[6, 12]]);
      }));

      function removeChildren(_x17, _x18, _x19) {
        return _removeChildren.apply(this, arguments);
      }

      return removeChildren;
    }()
  }, {
    key: "pushChildren",
    value: function () {
      var _pushChildren = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee17(_id, push, context) {
        var _this9 = this;

        var userId;
        return _regenerator.default.wrap(function _callee17$(_context17) {
          while (1) {
            switch (_context17.prev = _context17.next) {
              case 0:
                if (_id) {
                  _context17.next = 2;
                  break;
                }

                return _context17.abrupt("return", null);

              case 2:
                _id = _mongodb.ObjectID.createFromHexString(_id);
                userId = push.createdBy || push.updatedBy || null;
                delete push['updatedBy'];
                delete push['createdBy'];
                _context17.prev = 6;
                _context17.next = 9;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref8 = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee16(db) {
                    var response, value;
                    return _regenerator.default.wrap(function _callee16$(_context16) {
                      while (1) {
                        switch (_context16.prev = _context16.next) {
                          case 0:
                            _context16.next = 2;
                            return db.collection(_this9.collectionName).findOneAndUpdate({
                              _id: _id
                            }, {
                              $push: (0, _objectSpread2.default)({}, push)
                            }, {
                              returnOriginal: false
                            });

                          case 2:
                            response = _context16.sent;

                            if (!response.ok) {
                              _context16.next = 11;
                              break;
                            }

                            value = response.value;

                            if (!_this9.isHistoryEnabled()) {
                              _context16.next = 8;
                              break;
                            }

                            _context16.next = 8;
                            return (0, _history.default)(_this9.collectionName, value, _constants.HistoryCode.ELEMENT_CHILDREN_PUSHED, push, context, userId);

                          case 8:
                            return _context16.abrupt("return", value);

                          case 11:
                            throw new Error('Error during update');

                          case 12:
                          case "end":
                            return _context16.stop();
                        }
                      }
                    }, _callee16, this);
                  }));

                  return function (_x24) {
                    return _ref8.apply(this, arguments);
                  };
                }(), context);

              case 9:
                return _context17.abrupt("return", _context17.sent);

              case 12:
                _context17.prev = 12;
                _context17.t0 = _context17["catch"](6);
                throw new Error(_context17.t0);

              case 15:
              case "end":
                return _context17.stop();
            }
          }
        }, _callee17, this, [[6, 12]]);
      }));

      function pushChildren(_x21, _x22, _x23) {
        return _pushChildren.apply(this, arguments);
      }

      return pushChildren;
    }()
  }, {
    key: "remove",
    value: function () {
      var _remove = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee19(_id) {
        var _this10 = this;

        var context,
            soft,
            _args19 = arguments;
        return _regenerator.default.wrap(function _callee19$(_context19) {
          while (1) {
            switch (_context19.prev = _context19.next) {
              case 0:
                context = _args19.length > 1 && _args19[1] !== undefined ? _args19[1] : null;
                soft = _args19.length > 2 && _args19[2] !== undefined ? _args19[2] : false;

                if (_id) {
                  _context19.next = 4;
                  break;
                }

                return _context19.abrupt("return", null);

              case 4:
                _id = _mongodb.ObjectID.createFromHexString(_id);
                _context19.prev = 5;
                _context19.next = 8;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref9 = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee18(db) {
                    return _regenerator.default.wrap(function _callee18$(_context18) {
                      while (1) {
                        switch (_context18.prev = _context18.next) {
                          case 0:
                            if (soft) {
                              _context18.next = 4;
                              break;
                            }

                            _context18.next = 3;
                            return db.collection(_this10.collectionName).deleteOne({
                              _id: _id
                            });

                          case 3:
                            return _context18.abrupt("return", _context18.sent);

                          case 4:
                            if (!soft) {
                              _context18.next = 8;
                              break;
                            }

                            _context18.next = 7;
                            return db.collection(_this10.collectionName).findOneAndUpdate({
                              _id: _id
                            }, {
                              $set: {
                                deletedAt: (0, _now.default)()
                              }
                            }, {
                              returnOriginal: false
                            });

                          case 7:
                            return _context18.abrupt("return", _context18.sent);

                          case 8:
                          case "end":
                            return _context18.stop();
                        }
                      }
                    }, _callee18, this);
                  }));

                  return function (_x26) {
                    return _ref9.apply(this, arguments);
                  };
                }(), context);

              case 8:
                return _context19.abrupt("return", _context19.sent);

              case 11:
                _context19.prev = 11;
                _context19.t0 = _context19["catch"](5);
                throw new Error(_context19.t0);

              case 14:
              case "end":
                return _context19.stop();
            }
          }
        }, _callee19, this, [[5, 11]]);
      }));

      function remove(_x25) {
        return _remove.apply(this, arguments);
      }

      return remove;
    }()
  }, {
    key: "count",
    value: function () {
      var _count = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee21(query) {
        var _this11 = this;

        var context,
            _args21 = arguments;
        return _regenerator.default.wrap(function _callee21$(_context21) {
          while (1) {
            switch (_context21.prev = _context21.next) {
              case 0:
                context = _args21.length > 1 && _args21[1] !== undefined ? _args21[1] : null;
                _context21.prev = 1;
                _context21.next = 4;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref10 = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee20(db) {
                    var count;
                    return _regenerator.default.wrap(function _callee20$(_context20) {
                      while (1) {
                        switch (_context20.prev = _context20.next) {
                          case 0:
                            _context20.next = 2;
                            return db.collection(_this11.collectionName).find(query).count();

                          case 2:
                            count = _context20.sent;
                            return _context20.abrupt("return", count);

                          case 4:
                          case "end":
                            return _context20.stop();
                        }
                      }
                    }, _callee20, this);
                  }));

                  return function (_x28) {
                    return _ref10.apply(this, arguments);
                  };
                }(), context);

              case 4:
                return _context21.abrupt("return", _context21.sent);

              case 7:
                _context21.prev = 7;
                _context21.t0 = _context21["catch"](1);
                throw new Error(_context21.t0);

              case 10:
              case "end":
                return _context21.stop();
            }
          }
        }, _callee21, this, [[1, 7]]);
      }));

      function count(_x27) {
        return _count.apply(this, arguments);
      }

      return count;
    }()
  }, {
    key: "aggregate",
    value: function () {
      var _aggregate = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee23(query) {
        var _this12 = this;

        var context,
            _args23 = arguments;
        return _regenerator.default.wrap(function _callee23$(_context23) {
          while (1) {
            switch (_context23.prev = _context23.next) {
              case 0:
                context = _args23.length > 1 && _args23[1] !== undefined ? _args23[1] : null;
                _context23.prev = 1;
                _context23.next = 4;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref11 = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee22(db) {
                    var agreatetedData;
                    return _regenerator.default.wrap(function _callee22$(_context22) {
                      while (1) {
                        switch (_context22.prev = _context22.next) {
                          case 0:
                            _context22.next = 2;
                            return db.collection(_this12.collectionName).aggregate(query);

                          case 2:
                            agreatetedData = _context22.sent;
                            return _context22.abrupt("return", agreatetedData.toArray());

                          case 4:
                          case "end":
                            return _context22.stop();
                        }
                      }
                    }, _callee22, this);
                  }));

                  return function (_x30) {
                    return _ref11.apply(this, arguments);
                  };
                }(), context);

              case 4:
                return _context23.abrupt("return", _context23.sent);

              case 7:
                _context23.prev = 7;
                _context23.t0 = _context23["catch"](1);
                throw new Error(_context23.t0);

              case 10:
              case "end":
                return _context23.stop();
            }
          }
        }, _callee23, this, [[1, 7]]);
      }));

      function aggregate(_x29) {
        return _aggregate.apply(this, arguments);
      }

      return aggregate;
    }()
  }]);
  return CollectionFactory;
}();

var _default = CollectionFactory;
exports.default = _default;