"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _Factory = _interopRequireDefault(require("./Factory"));

var TrackedInvoiceFactory =
/*#__PURE__*/
function (_CollectionFacotry) {
  (0, _inherits2.default)(TrackedInvoiceFactory, _CollectionFacotry);

  function TrackedInvoiceFactory() {
    var _this;

    (0, _classCallCheck2.default)(this, TrackedInvoiceFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(TrackedInvoiceFactory).call(this));
    _this.collectionName = 'trackedinvoices';
    _this.validationRules = {
      create: [],
      update: []
    };
    return _this;
  }

  return TrackedInvoiceFactory;
}(_Factory.default);

var _default = TrackedInvoiceFactory;
exports.default = _default;