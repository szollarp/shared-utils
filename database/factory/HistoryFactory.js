"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _Factory = _interopRequireDefault(require("./Factory"));

var HistoryFactory =
/*#__PURE__*/
function (_CollectionFacotry) {
  (0, _inherits2.default)(HistoryFactory, _CollectionFacotry);

  function HistoryFactory() {
    var _this;

    (0, _classCallCheck2.default)(this, HistoryFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(HistoryFactory).call(this));
    _this.collectionName = 'history';
    _this.historyEnabled = false;
    _this.validationRules = {
      create: [],
      update: []
    };
    return _this;
  }

  return HistoryFactory;
}(_Factory.default);

var _default = HistoryFactory;
exports.default = _default;