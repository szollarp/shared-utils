"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _Factory = _interopRequireDefault(require("./Factory"));

var ReconciliationListFactory =
/*#__PURE__*/
function (_CollectionFacotry) {
  (0, _inherits2.default)(ReconciliationListFactory, _CollectionFacotry);

  function ReconciliationListFactory() {
    var _this;

    (0, _classCallCheck2.default)(this, ReconciliationListFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(ReconciliationListFactory).call(this));
    _this.collectionName = 'reconciliationlists';
    _this.validationRules = {
      create: [],
      update: []
    };
    return _this;
  }

  return ReconciliationListFactory;
}(_Factory.default);

var _default = ReconciliationListFactory;
exports.default = _default;