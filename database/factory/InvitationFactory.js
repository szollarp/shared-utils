"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _get2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/get"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _Factory = _interopRequireDefault(require("./Factory"));

var _propertyValidator = require("property-validator");

var InvitationFactory =
/*#__PURE__*/
function (_CollectionFacotry) {
  (0, _inherits2.default)(InvitationFactory, _CollectionFacotry);

  function InvitationFactory() {
    var _this;

    (0, _classCallCheck2.default)(this, InvitationFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(InvitationFactory).call(this));
    _this.collectionName = 'invitations';
    _this.validationRules = {
      create: [(0, _propertyValidator.presence)('permissions'), (0, _propertyValidator.presence)('status'), (0, _propertyValidator.email)('email')]
    };
    return _this;
  }

  (0, _createClass2.default)(InvitationFactory, [{
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(args, context) {
        var validation, isSet;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                validation = (0, _propertyValidator.validate)(args, this.validationRules.create);

                if (!(!validation.valid || !context)) {
                  _context.next = 4;
                  break;
                }

                throw new Error('Invalid data');

              case 4:
                _context.next = 6;
                return this.getOne({
                  email: args.email
                }, context);

              case 6:
                isSet = _context.sent;

                if (!isSet) {
                  _context.next = 9;
                  break;
                }

                throw new Error('The user has been already invited. Please click on Resend invitation to send the email again.');

              case 9:
                _context.next = 11;
                return (0, _get2.default)((0, _getPrototypeOf2.default)(InvitationFactory.prototype), "create", this).call(this, args, context);

              case 11:
                return _context.abrupt("return", _context.sent);

              case 14:
                _context.prev = 14;
                _context.t0 = _context["catch"](0);
                throw new Error(_context.t0);

              case 17:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 14]]);
      }));

      function create(_x, _x2) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }]);
  return InvitationFactory;
}(_Factory.default);

var _default = InvitationFactory;
exports.default = _default;