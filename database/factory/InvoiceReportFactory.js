"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _now = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/date/now"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/objectSpread"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _Factory = _interopRequireDefault(require("./Factory"));

var _handler = require("../handler");

var InvoiceReportFactory =
/*#__PURE__*/
function (_CollectionFacotry) {
  (0, _inherits2.default)(InvoiceReportFactory, _CollectionFacotry);

  function InvoiceReportFactory() {
    var _this;

    (0, _classCallCheck2.default)(this, InvoiceReportFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(InvoiceReportFactory).call(this));
    _this.collectionName = 'invoicereports';
    _this.validationRules = {
      create: [],
      update: []
    };
    return _this;
  }

  (0, _createClass2.default)(InvoiceReportFactory, [{
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(args, context) {
        var _this2 = this;

        var invoiceResponseData;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (context) {
                  _context2.next = 2;
                  break;
                }

                throw new Error('Invalid data');

              case 2:
                _context2.prev = 2;
                invoiceResponseData = (0, _objectSpread2.default)({
                  createdAt: (0, _now.default)(),
                  createdBy: null,
                  softwareId: null
                }, args, {
                  receives: [],
                  sends: [],
                  errorResults: [],
                  warningResults: []
                });
                _context2.next = 6;
                return (0, _handler.handleDbQuery)(
                /*#__PURE__*/
                function () {
                  var _ref = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee(db) {
                    var response;
                    return _regenerator.default.wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            _context.next = 2;
                            return db.collection(_this2.collectionName).insertOne(invoiceResponseData);

                          case 2:
                            response = _context.sent;
                            return _context.abrupt("return", response.ops.pop());

                          case 4:
                          case "end":
                            return _context.stop();
                        }
                      }
                    }, _callee, this);
                  }));

                  return function (_x3) {
                    return _ref.apply(this, arguments);
                  };
                }(), context);

              case 6:
                return _context2.abrupt("return", _context2.sent);

              case 9:
                _context2.prev = 9;
                _context2.t0 = _context2["catch"](2);
                throw new Error(_context2.t0);

              case 12:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[2, 9]]);
      }));

      function create(_x, _x2) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }]);
  return InvoiceReportFactory;
}(_Factory.default);

var _default = InvoiceReportFactory;
exports.default = _default;