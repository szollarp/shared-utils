"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/objectSpread"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _get2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/get"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _propertyValidator = require("property-validator");

var _Factory = _interopRequireDefault(require("./Factory"));

var _UserFactory = _interopRequireDefault(require("./UserFactory"));

var FilterFactory =
/*#__PURE__*/
function (_CollectionFactory) {
  (0, _inherits2.default)(FilterFactory, _CollectionFactory);

  function FilterFactory() {
    var _this;

    (0, _classCallCheck2.default)(this, FilterFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(FilterFactory).call(this));
    _this.collectionName = 'filters';
    _this.userFactory = new _UserFactory.default();
    _this.validationRules = {
      create: [(0, _propertyValidator.presence)('name'), (0, _propertyValidator.presence)('enabled'), (0, _propertyValidator.presence)('conditions')],
      update: [(0, _propertyValidator.presence)('name'), (0, _propertyValidator.presence)('enabled'), (0, _propertyValidator.presence)('conditions')]
    };
    return _this;
  }

  (0, _createClass2.default)(FilterFactory, [{
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(args, user, context) {
        var filter;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (context) {
                  _context.next = 2;
                  break;
                }

                throw new Error('Invalid data');

              case 2:
                _context.next = 4;
                return this.userFactory.getOne({
                  _id: user._id
                });

              case 4:
                user = _context.sent;

                if (user) {
                  _context.next = 7;
                  break;
                }

                throw new Error('Invalid user');

              case 7:
                _context.prev = 7;
                filter = (0, _objectSpread2.default)({}, args, {
                  createdBy: user._id
                });
                _context.next = 11;
                return (0, _get2.default)((0, _getPrototypeOf2.default)(FilterFactory.prototype), "create", this).call(this, (0, _objectSpread2.default)({}, filter), context);

              case 11:
                return _context.abrupt("return", _context.sent);

              case 14:
                _context.prev = 14;
                _context.t0 = _context["catch"](7);
                throw new Error(_context.t0);

              case 17:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[7, 14]]);
      }));

      function create(_x, _x2, _x3) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }, {
    key: "update",
    value: function () {
      var _update = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(id, args, user, context) {
        var name, enabled, conditions, filter;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                name = args.name, enabled = args.enabled, conditions = args.conditions;

                if (context) {
                  _context2.next = 3;
                  break;
                }

                throw new Error('Invalid data');

              case 3:
                _context2.next = 5;
                return this.userFactory.getOne({
                  _id: user._id
                });

              case 5:
                user = _context2.sent;

                if (user) {
                  _context2.next = 8;
                  break;
                }

                throw new Error('Invalid user');

              case 8:
                _context2.prev = 8;
                filter = {
                  enabled: enabled,
                  updatedBy: user._id
                };

                if (name && conditions) {
                  filter.name = name;
                  filter.conditions = conditions;
                }

                _context2.next = 13;
                return (0, _get2.default)((0, _getPrototypeOf2.default)(FilterFactory.prototype), "update", this).call(this, id, (0, _objectSpread2.default)({}, filter), context);

              case 13:
                return _context2.abrupt("return", this.get({}, context));

              case 16:
                _context2.prev = 16;
                _context2.t0 = _context2["catch"](8);
                throw new Error(_context2.t0);

              case 19:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[8, 16]]);
      }));

      function update(_x4, _x5, _x6, _x7) {
        return _update.apply(this, arguments);
      }

      return update;
    }()
  }, {
    key: "remove",
    value: function () {
      var _remove = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(id, user, context) {
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                if (context) {
                  _context3.next = 2;
                  break;
                }

                throw new Error('Invalid data');

              case 2:
                _context3.next = 4;
                return this.userFactory.getOne({
                  _id: user._id
                });

              case 4:
                user = _context3.sent;

                if (user) {
                  _context3.next = 7;
                  break;
                }

                throw new Error('Invalid user');

              case 7:
                _context3.prev = 7;
                _context3.next = 10;
                return (0, _get2.default)((0, _getPrototypeOf2.default)(FilterFactory.prototype), "remove", this).call(this, id, context);

              case 10:
                return _context3.abrupt("return", this.get({}, context));

              case 13:
                _context3.prev = 13;
                _context3.t0 = _context3["catch"](7);
                throw new Error(_context3.t0);

              case 16:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[7, 13]]);
      }));

      function remove(_x8, _x9, _x10) {
        return _remove.apply(this, arguments);
      }

      return remove;
    }()
  }]);
  return FilterFactory;
}(_Factory.default);

var _default = FilterFactory;
exports.default = _default;