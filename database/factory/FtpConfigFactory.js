"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _propertyValidator = require("property-validator");

var _Factory = _interopRequireDefault(require("./Factory"));

var FtpConfigFactory =
/*#__PURE__*/
function (_CollectionFacotry) {
  (0, _inherits2.default)(FtpConfigFactory, _CollectionFacotry);

  function FtpConfigFactory() {
    var _this;

    (0, _classCallCheck2.default)(this, FtpConfigFactory);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(FtpConfigFactory).call(this));
    _this.collectionName = 'ftpconfigs';
    _this.validationRules = {
      create: [(0, _propertyValidator.presence)('softwareId'), (0, _propertyValidator.presence)('username'), (0, _propertyValidator.presence)('password')],
      update: [(0, _propertyValidator.presence)('username'), (0, _propertyValidator.presence)('password')]
    };
    return _this;
  }

  return FtpConfigFactory;
}(_Factory.default);

var _default = FtpConfigFactory;
exports.default = _default;