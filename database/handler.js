"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.handleDbQuery = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _mongodb = require("mongodb");

var _config = _interopRequireDefault(require("config"));

var handleDbQuery =
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(callback) {
    var database,
        dbName,
        client,
        db,
        response,
        _args = arguments;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            database = _args.length > 1 && _args[1] !== undefined ? _args[1] : null;
            dbName = !database ? _config.default.get('database.database') : database;
            _context.prev = 2;
            _context.next = 5;
            return _mongodb.MongoClient.connect(_config.default.get('database.host'));

          case 5:
            client = _context.sent;
            db = client.db(dbName);
            _context.next = 9;
            return callback(db);

          case 9:
            response = _context.sent;

            if (client) {
              client.close();
            }

            return _context.abrupt("return", response);

          case 14:
            _context.prev = 14;
            _context.t0 = _context["catch"](2);
            console.error(_context.t0.toString());
            throw new Error(_context.t0);

          case 18:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[2, 14]]);
  }));

  return function handleDbQuery(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.handleDbQuery = handleDbQuery;