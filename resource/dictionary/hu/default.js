"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  /* NAV manageInvoice ERROR */
  'INVALID_REQUEST': {
    message: 'Nem séma-valid XML a request body-ban',
    issue: 'Felhasználói adatok ellenőrzése, javítása',
    whatToDo: 'A beküldött XML - válaszban felsorolt - elemei sértik az invoiceApi.xsd megkötéseit, javítani kell.'
  },
  'INVALID_SECURITY_USER': {
    message: 'A kérésben hibás login + passwordHash pár',
    issue: 'Felhasználói azonosítási adatok ellenőrzése, javítása',
    whatToDo: 'Számos esetben jelentkezhet a hibaüzenet. Lehetséges okok: a megadott login névvel nem létezik felhasználó, vagy nem helyes a jelszava, vagy a login + passwordHash pár szemantikailag helyes, de a jelszóhash rosszul kerül kiszámításra a kliens oldalán. Meg kell győződni az adatok és a hashelés helyességéről, szükség esetén fel kell venni a kapcsolatot a technikai felhasználót birtokló adózóval.'
  },
  'INVALID_CUSTOMER': {
    message: 'A kérésben hibás a taxNumber',
    issue: 'Adószám ellenőrzése, javítása',
    whatToDo: 'A user tagban megadott adószám vagy nem létezik, vagy a státusza nem engedi a számlaműveletek végzését. Meg kell győződni az adatok helyességéről, szükség esetén fel kell venni a kapcsolatot az elsődleges felhasználókkal (adózó ügyfélkapuval rendelkező törvényes képviselője vagy álllandó meghatalmazottja lehet)'
  },
  'INVALID_USER_RELATION': {
    message: 'A kérésben szereplő entitások között nincs kapcsolat',
    issue: 'Adószám és technikai felhasználó ellenőrzése, javítása',
    whatToDo: 'A megadott adószámhoz nem tartozik a megadott login névvel technikai felhasználó, vagy a felhasználó státusza már nem engedélyezi a művelet elvégzését. Meg kell győződni az adatok helyességéről, szükség esetén fel kell venni a kapcsolatot az elsődleges felhasználókkal (adózó  ügyfélkapuval rendelkező törvényes képviselője vagy álllandó meghatalmazottja lehet)'
  },
  'FORBIDDEN': {
    message: 'A kérésben szereplő technikai felhasználó nem jogosult az endpoint szolgáltatását hívni',
    issue: 'Felhasználó jogosultságainak ellenőrzése',
    whatToDo: 'A technikai felhasználók jogosultságait az adózó elsődleges felhasználói osztják ki. Szükség esetén fel kell venni a kapcsolatot az elsődleges felhasználókkal (adózó ügyfélkapuval rendelkező törvényes képviselője vagy álllandó meghatalmazottja lehet)'
  },
  'REQUEST_ID_NOT_UNIQUE': {
    message: 'A kérésben szereplő requestId nem egyedi',
    issue: 'RequestId egyediségének biztosítása',
    whatToDo: 'A kérésben szereplő adószámra a megadott requestId-t már felhasználták. Az egyediség miatt új id megadása szükséges.'
  },
  'INVALID_REQUEST_SIGNATURE': {
    message: 'A kérésben szereplő requestSignature hibás',
    issue: 'RequestSignature adatainak ellenőrzése',
    whatToDo: 'A szerver oldalon elvégzett requestSignature számítás nem egyezik meg a kliens oldalon kiszámított értékkel. A számítás módjáról olvassa el a requestSignature számítása című fejezetet.'
  },
  'INDEX_NOT_SEQUENTIAL': {
    message: 'A kérésben szereplő index nem sorfolytonos',
    issue: 'Beküldött számlák indexeinek ellenőrzése',
    whatToDo: 'Az invoiceOperations listaelem alatt lévő indexeknek sorfolytonosan emelkedőnek kell lenniük. Ellenőrizni kell, hogy a kérésben nincs helytelen sorrendű, hézagos, vagy 1-nél többször előforduló index.'
  },
  'INVALID_EXCHANGE_TOKEN': {
    message: 'A kérésben szereplő adatszolgáltatási token érvénytelen',
    issue: 'Token érvényességének biztosítása',
    whatToDo: 'Számos esetben jelentkezhet a hibaüzenet. Lehetséges okok: a megadott token nem található a rendszerben, a token már lejárt, a token nem a megadott adózóra lett kiállítva, vagy a kliens oldali AES dekódolás hiányzik, esetleg hibás. Meg kell győződni az adatok és a dekódolás helyességéről.'
  },
  'INVALID_OPERATION': {
    message: 'A kérésben szereplő operációk típusa nem megengedett',
    issue: 'Kérésben szereplő operáció ellenőrzése, javítása',
    whatToDo: 'Ha a kérésben technicalAnnulment = true szerepel, akkor minden operation tag értéke ANNUL lehet csak. Javítani kell!'
  },
  'OPERATION_FAILED': {
    message: 'Váratlan feldolgozási hiba',
    issue: 'Számlaművelet megismétlése, vagy javítása',
    whatToDo: 'Az aszinkron műveletek hibatűrése szerver oldalon biztosított. A szóban forgó hiba csak szinkron hívásoknál jelentkezhet, ilyenkor a műveletet kis idő elteltével meg kell ismételni. Ha az éles rendszerben többszöri próbálkozásra sem sikerül a művelet, fel kell venni a kapcsolatot a NAV helpdeskkel, azonban célszerű előtte tájékozódni, hogy a portál oldalon nincs-e üzemszünettel, üzemzavarral kapcsolatos tájékoztatás. Felhívjuk a figyelmet, hogy a felhasználói teszt rendszerben nincs garantált rendelkezésre állás, ezért kérjük, hogy a tesztrendszer hibáit ne jelentsék be!'
  },

  /* NAV queryInvoiceStatus ERROR */
  'SUPPLIER_TAX_NUMBER_MISSING': {
    message: 'Hiányzik az eladó adószáma',
    issue: 'Eladó adószámának pótlása',
    whatToDo: 'Kérjük ellenőrizze a kiállított számlát. Amennyiben a számláról hiányzik a társaság adószáma, a számla módosítására van szükség. A hiba eredhet abból is, ha a számlát leíró xml fájlban hiányzik az eladó adószáma. Bármelyik okból merült fel a hibás adatszolgáltatás, az adatszolgáltatást meg kell ismételni a helyes adatokkal a hiba észlelésétől számított 3 munkanapon belül. Ha a számlán hiányzik az eladó adószáma, a számla érvénytelenítésre van szükség, majd az érvénytelenítő számla és helyesen kibocsátott számla újbóli beküldésére. Azonban ha a hiba az xml fájl helytelenségéből ered akkor az xml fájl javítására van szükség. Ha a hiba elhárítása a 3 munkanapot meghaladja, kézi feltöltésre van lehetőség az adóhatóság honlapján (https://kobak.nav.gov.hu/). Felhívjuk a figyelmet arra, hogy jelenleg teszt időszak folyik, így a fent említett szabályozás csak az "éles" indulás után válik kötelezővé.'
  },
  'SUPPLIER_TAX_NUMBER_MISMATCH': {
    message: 'Az eladó adószáma eltér a kérésben szereplő adószámtól.',
    issue: 'Számlán szereplő adószám módosítása az eladó adószámára',
    whatToDo: 'A hiba akkor jelentkezik, ha a számlát leíró xml fájlban az eladó adószáma nem egyezik meg a rendszerben megadott adószámmal. Kérjük ellenőrizze a számlát leíró xml fájlban az eladó adószámát, ha ott helyes érték szerepel akkor a rendszerben (lásd beállítások menüpont) másik társaság adatait adta meg. A helytelen értéket javítani szükséges vagy a számlát leíró xml fájlban vagy a rendszerben megadott cégadatoknál. Ha az xml fájlban merült fel a hibás adatszolgáltatás, az adatszolgáltatást meg kell ismételni a helyes adatokkal a hiba észlelésétől számított 3 munkanapon belül. Ha a hiba elhárítása a 3 munkanapot meghaladja, kézi feltöltésre van lehetőség az adóhatóság honlapján (https://onlineszamla-test.nav.gov.hu/login). Felhívjuk a figyelmet arra, hogy jelenleg teszt időszak folyik, így a fent említett szabályozás csak az "éles" indulás után válik kötelezővé.'
  },
  'INVALID_TAX_NUMBER': {
    message: 'Érvénytelen adószám az invoiceHead-ben',
    issue: 'Érvényes adószám megadása az invoiceHead-ben',
    whatToDo: 'A számlát leíró beküldött xml fájlban az eladó adószáma helytelen az Adóhatóság adatbázisa alapján vagy az adószám nem létezik. Az eladó adószámának javítására van szükség az xml fájl-ban. A javítás után az adatszolgáltatást meg kell ismételni a hiba észlelésétől számított 3 munkanapon belül. Ha a hiba elhárítása a 3 munkanapot meghaladja, kézi feltöltésre van lehetőség az adóhatóság honlapján (https://onlineszamla-test.nav.gov.hu/login). Felhívjuk a figyelmet arra, hogy jelenleg teszt időszak folyik, így a fent említett szabályozás csak az "éles" indulás után válik kötelezővé.'
  },
  'INVOICE_NUMBER_NOT_UNIQUE': {
    message: 'Nem egyedi számla sorszám az invoiceHeadben',
    issue: 'Egyedi sorszám használata az invoiceHeadben',
    whatToDo: 'A megadott számla sorszáma már szerepel az Adóhatóság rendszerében. Kétszer ugyan olyan sorszámú számlát nem lehet beküldeni, ez alól kivételt képez ha az ugyan olyan sorszámú számlát technikailag érvénytelenítették. Ha a számlát valamiért a rendszer duplán küldte be az adóhatóság felé, ebben az esetben további teendője nincs. Azonban, ha az előző ugyan ilyen sorszámmal beküldött számlát technikailag érvénytelenítette, de azt nem hagyta jóvá az Adóhatóság rendszerében, akkor jóváhagyás szükséges, majd az adatszolgáltatás megismétlése a javított adatokkal a hiba észlelésétől számított 3 munkanapon belül.Technikai érvénytelenítés jelentése: technikai érvénytelenítés kizárólag olyan korábbi adatszolgáltatásra vonatkozóan teljesíthető, amelyre már sikeres visszaigazoló nyugta („OK”), vagy kizárólag figyelmeztetéseket („WARN”) tartalmazó válaszüzenet érkezett. Technika érvénytelenítésre akkor van szükség, ha a korábbi adatszolgáltatásunkat szeretnénk helyesbíteni.'
  },
  'LINE_NUMBER_NOT_SEQUENTIAL': {
    message: 'Nem sorfolytonos számozás az invoiceLines listaelemen belül.',
    issue: 'Sorfolytonos számozás használata az invoiceLines listaelemen belül.',
    whatToDo: 'A számlát leíró xml fájlban ( az InvoiceLines listaelemen belül) lévő sorszám (lineNumber) elemeknek sorfolytonosan emelkedőnek kell lenniük. Ellenőrizni kell, hogy a kérésben nincs helytelen sorrendű, hézagos, vagy 1-nél többször előforduló lineNumber. A hiba nem a hibás számlázásból adódik, hanem a számla adattartalmát leíró xml fájl helytelenül van lefejlesztve. Forduljon a számlázóprogramját fejlesztő programozóhoz.'
  },
  'INVOICE_LINE_MISSING': {
    message: 'A számla nem tartalmaz tételt',
    issue: 'Számla tételeinek pótlása',
    whatToDo: 'Az alapszámláról nem szolgáltatható adat számla tétel nélkül. Kérjük ellenőrizze a számlát leíró xml-t. Alapszámla esetében az InvoiceLines elem kötelező. Ha a kiállított számla tartalmaz tételt azonban az xml fájl nem, forduljon a számlázóprogramját fejlesztő programozóhoz. A hiba észlelésétől 3 munaknap áll rendelkezésre a hiba kijavítására a jogszabály szerint. Ha a hiba elhárítása a 3 munkanapot meghaladja, kézi feltöltésre van lehetőség az adóhatóság honlapján (https://onlineszamla-test.nav.gov.hu/login). Felhívjuk a figyelmet arra, hogy jelenleg teszt időszak folyik, így a fent említett szabályozás csak az "éles" indulás után válik kötelezővé.'
  },
  'INVALID_INVOICE_REFERENCE': {
    message: 'Hibás a számla hivatkozás módosítás vagy érvénytelenítés esetén',
    issue: 'Számlahivatkozás ellenőrzése módosítás vagy érvénytelenítés esetén',
    whatToDo: 'A módosítás vagy érvénytelenítés (sztornózás) által hivatkozott számla nem található meg az adózó számlái között az adóhatóság rendszerében, és a számlát leíró xml fájlban nem jelölték, hogy a módosításhoz nem tartozik korábbi adatszolgáltatás. (invoiceReference-ben a modifyWithoutMaster tag értéke false) Javítani kell a hivatkozott számla sorszámot, vagy a modifyWithoutMaster elemet true értékkel kell beküldeni. Kérjük ellenőrizze a kiállított számlán és az xml-fájlban lévő hivatkozást, ha a hivatkozás helyes akkor nem jelölték az xml-ben, hogy a módosításhoz nem tartozik korábbi adatszolgáltatás. Felhívnánk a figyelmet arra, hogy az Adóhatóság által közéttet leírás szerint nincs lehetőség módosító okirat módosítására, az újabb módosítás az eredeti számla módosításának tekintendő és ilyen módon is kell róla adatot szolgáltatni. így a hiba eredhet abból is, hogy a módosító okirat egy korábbi módosításra és nem az eredeti számlára hivatkozik. A hiba észlelésétől 3 munaknap áll rendelkezésre a hiba kijavítására a jogszabály szerint. Ha a hiba elhárítása a 3 munkanapot meghaladja, kézi feltöltésre van lehetőség az adóhatóság honlapján (https://onlineszamla-test.nav.gov.hu/login). Felhívjuk a figyelmet arra, hogy jelenleg teszt időszak folyik, így a fent említett szabályozás csak az "éles" indulás után válik kötelezővé. Valamint a tesztidőszak alatt ezt a hibaüzenetet kapjuk ha a modifyWithoutMaster elemet "true" értékkel láttuk el, mivel az Adóhatóság rendszere jelenleg nem veszi figylembe a beküldött számlákat'
  },
  'INVALID_ANNULMENT_REFERENCE': {
    message: 'Hibás a számla hivatkozás technikai érvénytelenítés esetén',
    issue: 'Számlahivatkozás ellenőrzése technikai érvénytelenítés esetén',
    whatToDo: 'A technikai érvénytelenítés olyan számla sorszámra hivatkozik az érvénytelenítést leíró xml-ben (azon belül annulmentReference-ben), mely az adózó számlái között nem található meg az adóhatóság rendszerében. Ellenőrizni kell a hivatkozott számla sorszámot. Technikai érvénytelenítés jelentése: technikai érvénytelenítés kizárólag olyan korábbi adatszolgáltatásra vonatkozóan teljesíthető, amelyre már sikeres visszaigazoló nyugta („OK”), vagy kizárólag figyelmeztetéseket („WARN”) tartalmazó válaszüzenet érkezett. Technika érvénytelenítésre akkor van szükség, ha a korábbi adatszolgáltatásunkat szeretnénk helyesbíteni. A technikai érvénytelenítést az Adóhatóság rendszerében jóvá kell hagyni a gép-gép interfésszel való beküldés során is, ezért célszerűbb az Adóhatóság rendszerében technikailag érvényteleníteni az adatszolgáltatást is. Felhívjuk a figyelmet, hogy jelenleg az Adóhatóság rendszere tesztrendszerként üzemel, ezért a technikai érvénytelenítés során beküldött xml-ek, hibaüzenettel érkeznek vissza mert az Adóhatóság rendszere jelenleg nem veszi figyelembe a beküldött számlákat.'
  },

  /* NAV queryInvoiceStatus WARN */
  'SUPPLIER_CUSTOMER_MATCH': {
    message: 'Az eladó és vevő adataiban azonosság található',
    issue: 'Az eladó és vevő adatainak ellenőrzése',
    whatToDo: 'Az invoiceHead-ben szereplő valamely supplier és customer adat helytelenül megyezik. A kérdéses értékek a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'CUSTOMER_FISCAL_MATCH': {
    message: 'A vevő és a pénzügyi képviselő adataiban azonosság található',
    issue: 'A vevő és a pénzügyi képviselő adatainak ellenőrzése',
    whatToDo: 'Az invoiceHead-ben szereplő valamely customer és fiscalRepresentative adat helytelenül megyezik. A kérdéses értékek a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'INCORRECT_VAT_CODE': {
    message: 'Helytelen ÁFA kód',
    issue: 'Helytelen ÁFA kód javítása',
    whatToDo: 'A kérés valamely adata miatt az ÁFA kód helytelen. A kérdéses értékek és okok a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'INCORRECT_COUNTY_CODE': {
    message: 'Helytelen megyekód',
    issue: 'Helytelen megyekód javítása',
    whatToDo: 'A kérés valamely TaxNumberType szerkezet szerinti tagjában lévő countyCode helytelen. A kérdéses értékek és okok a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'INCORRECT_COUNTRY_CODE': {
    message: 'Helytelen országkód',
    issue: 'Helytelen országkód javítása',
    whatToDo: 'Az invoiceHead-ben szereplő valamely országkód az ISO 3166 alpha-2 szabvány szerint helytelen. A kérdéses értékek és okok a businessValidationMessages/message tagban visszaadásra kerülnek'
  },
  'INCORRECT_CITY_ZIP_CODE': {
    message: 'Helytelen irányítószám és településnév pár',
    issue: 'Helytelen irányítószám és településnév pár javítása',
    whatToDo: 'A kérésben szereplő valamely AddressType szerinti tagjában az irányítószám és településnév pár helytelen. Az adatokat az irányítószám törzs alapján kell megadni. A kérdéses értékek és okok a businessValidationMessages/message tagban visszaadásra kerülnek'
  },
  'INCORRECT_PRODUCT_CODE': {
    message: 'Helytelen termékkód',
    issue: 'Helytelen termékkód javítása',
    whatToDo: 'A line elemben szereplő valamely productCodeValue nem felel meg a productCodeCategory-ban definiált nómenklatúrának (ha az nem saját vagy egyéb megjelölésű kód). A kérdéses értékek és okok a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'INCORRECT_DATE': {
    message: 'Helytelen dátum adat',
    issue: 'Helytelen dátum adat javítása',
    whatToDo: 'A kérésben szereplő valamely dátum egy másik dátumhoz képest helytelen értéket tartalmaz. A kérdéses értékek és okok a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'MISSING_HEAD_DATA': {
    message: 'Hiányzó számlafej adat',
    issue: 'Hiányzó számlafej adat pótlása',
    whatToDo: 'A számla valamely adatai alapján az invoiceHead valamely eleme hiányzik. A hiányzó értékek a businessValidationMessages/message tagban visszaadásra kerülnek'
  },
  'MISSING_LINE_DATA': {
    message: 'Hiányzó számla tétel adat',
    issue: 'Hiányzó számla tétel adat javítása',
    whatToDo: 'A számla valamely adatai alapján az invoiceLines valamely eleme hiányzik. A hiányzó értékek a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'MISSING_PRODUCT_FEE_DATA': {
    message: 'Hiányzó termékdíj adat',
    issue: 'Hiányzó termékdíj adat pótlása',
    whatToDo: 'A számla valamely adatai alapján a productFeeSummary valamely eleme hiányzik. A hiányzó értékek a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'MISSING_SUMMARY_DATA': {
    message: 'Hiányzó összesítő adatok',
    issue: 'Hiányzó összesítő adatok pótlása',
    whatToDo: 'A számla valamely adatai alapján az invoiceSummary valamely eleme hiányzik. A hiányzó értékek a businessValidationMessages/message tagban visszaadásra kerülnek'
  },
  'INCORRECT_HEAD_DATA': {
    message: 'Helytelen számlafej adat',
    issue: 'Helytelen számlafej adat javítása',
    whatToDo: 'A számla valamely összefüggése alapján az invoiceHead valamely eleme helytelen. A hiányzó értékek a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'INCORRECT_LINE_DATA': {
    message: 'Helytelen számla tétel adat',
    issue: 'Helytelen számla tétel adat javítása',
    whatToDo: 'A számla valamely összefüggése alapján az invoiceLines valamely eleme helytelen. A hiányzó értékek a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'INCORRECT_PRODUCT_FEE_DATA': {
    message: 'Helytelen termékdíj adat',
    issue: 'Helytelen termékdíj adat javítása',
    whatToDo: 'A számla valamely összefüggése alapján a productFeeSummary valamely eleme helytelen. A hiányzó értékek a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'INCORRECT_SUMMARY_DATA': {
    message: 'Helytelen összesítő adatok',
    issue: 'Helytelen összesítő adatok javítása',
    whatToDo: 'A számla valamely összefüggése alapján az invoiceSummary valamely eleme helytelen. A hiányzó értékek a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'INCORRECT_LINE_CALCULATION': {
    message: 'Helytelen tétel számítás',
    issue: 'Helytelen tétel számítás javítása',
    whatToDo: 'A számla valamely adatai alapján az invoiceLines elemben számítási hiba can. A helytelen értékek a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'INCORRECT_PRODUCT_FEE_CALCULATION': {
    message: 'Helytelen termékdíj számítás',
    issue: 'Helytelen termékdíj számítás javítása',
    whatToDo: 'A számla valamely adatai alapján a ProductFeeSummary elemben számítási hiba can. A helytelen értékek a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'INCORRECT_SUMMARY_CALCULATION': {
    message: 'Helytelen összesítés számítás',
    issue: 'Helytelen összesítés számítás javítása',
    whatToDo: 'A számla valamely adatai alapján az invoiceSummary elemben számítási hiba can. A helytelen értékek a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'LINE_SUMMARY_TYPE_MISMATCH': {
    message: 'Sor-összesítés típuseltérés',
    issue: 'Sor-összesítés típuseltérés javítása',
    whatToDo: 'A számla valamely sorának olyan összesítése van, amely az adott számlatípusban helytelen. (normál számlasornak egyszerűsített összesítése, és/vagy fordítva) A helytelen értékek a businessValidationMessages/message tagban visszaadásra kerülnek.'
  },
  'ISSUE_DATE_TIMESTAMP_MISMATCH': {
    message: 'Dátum-időbélyeg eltérés',
    issue: 'Dátum-időbélyeg eltérés javítása',
    whatToDo: 'Módosítás vagy érvénytelenítés esetén az invoiceReference elemben a módosító okirat kiállítási dátuma és az időbélyeg eltérő értéket tartalmaz.'
  },
  'INCORRECT_INVOICE_REFERENCE': {
    message: 'Hibás számla hivatkozás módosítás vagy érvénytelenítés esetén',
    issue: 'Hibás számla hivatkozás javítása módosítás vagy érvénytelenítés esetén',
    whatToDo: 'A módosítás vagy érvénytelenítés olyan számla sorszámra hivatkozik az originalInvoiceNumber-ben, mely az adózó számlái között nem található meg a rendszerben. Ellenőrizni kell a hivatkozott számla sorszámot (csak modifyWithoutMaster true esetén kerül visszaadásra)'
  },
  'INCORRECT_LINE_REFERENCE': {
    message: 'Hibás sor hivatkozás módosítás vagy érvénytelenítés esetén',
    issue: 'Hibás sor hivatkozás javítása módosítás vagy érvénytelenítés esetén',
    whatToDo: 'A módosítás vagy érvénytelenítés olyan sorszámra hivatkozik az lineNumberReference-ben, mely a megjelölt számlán nem létezik, vagy tétel hozzáadás esetén olyan sorra hivatkozik, amely az alapszámlán már létezik. Ellenőrizni kell a hivatkozott sorszámot.'
  },

  /* IRINA által státuszolt NAV validációs hiba */
  'SCHEMA_VALIDATION_ERROR': {
    message: 'XML sémának ellentmondó adattartalom',
    issue: 'XML sémának ellentmondó adattartalom javítása',
    whatToDo: 'A validációs hibák segítséget nyújthatnak a számla XML tartalmában előforduló révénytelen értékek javításában'
  }
};
exports.default = _default;