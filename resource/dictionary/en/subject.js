"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  'HIGH': 'Your invoice got a HIGH priority issue',
  'LOW': 'Your invoice got a LOW priority issue',
  'IRINA_FORGOT_PASSWORD_LINK_SENT': 'Renewing your password',
  'IRINA_PASSWORD_CHANGE': 'Your password has been successfully renewed',
  'IRINA_INVITATION_SUCCESS': 'You have a new invitation',
  'IRINA_INVITATION_SUCCESS_WITH_USER': 'You have a new invitation',
  'IRINA_REGISTRATION_SUCCESS': 'Your registration was successful'
};
exports.default = _default;