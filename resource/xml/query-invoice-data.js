"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = "\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<QueryInvoiceDataRequest xmlns=\"http://schemas.nav.gov.hu/OSA/1.0/api\">\n\t<header>\n\t\t<requestId></requestId>\n\t\t<timestamp></timestamp>\n\t\t<requestVersion>1.0</requestVersion>\n\t\t<headerVersion>1.0</headerVersion>\n\t</header>\n\t<user>\n\t\t<login></login>\n\t\t<passwordHash></passwordHash>\n\t\t<taxNumber></taxNumber>\n\t\t<requestSignature></requestSignature>\n\t</user>\n\t<software></software>\n\t<page></page>\n\t<queryParams>\n\t\t<invoiceIssueDateFrom></invoiceIssueDateFrom>\n\t\t<invoiceIssueDateTo></invoiceIssueDateTo>\n\t</queryParams>\n</QueryInvoiceDataRequest>";
exports.default = _default;