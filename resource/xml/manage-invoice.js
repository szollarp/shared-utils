"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = "\n  <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n  <ManageInvoiceRequest xmlns=\"http://schemas.nav.gov.hu/OSA/1.0/api\">\n    <header>\n      <requestId></requestId>\n      <timestamp></timestamp>\n      <requestVersion>1.0</requestVersion>\n      <headerVersion>1.0</headerVersion>\n    </header>\n    <user>\n      <login></login>\n      <passwordHash></passwordHash>\n      <taxNumber></taxNumber>\n      <requestSignature></requestSignature>\n    </user>\n    <software></software>\n    <exchangeToken></exchangeToken>\n    <invoiceOperations>\n      <technicalAnnulment>false</technicalAnnulment>\n      <compressedContent>false</compressedContent>\n      <invoiceOperation>\n        <index>1</index>\n        <operation>CREATE</operation>\n        <invoice></invoice>\n      </invoiceOperation>\n    </invoiceOperations>\n  </ManageInvoiceRequest>\n";
exports.default = _default;