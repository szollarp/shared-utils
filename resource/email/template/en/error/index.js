"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _details = _interopRequireDefault(require("./details"));

var _default = function _default(notificationObject) {
  return "<p>Dear Partner,</p>\n    <p>The following error occured during an invoice transaction:</p>\n    <p>Error code: ".concat(notificationObject.errorCode, "</p>\n    <p>Error message: ").concat(notificationObject.message, "</p>\n    <p>Error source: ").concat(notificationObject.source, "</p>\n    ").concat((0, _details.default)(notificationObject), "\n    ");
};

exports.default = _default;