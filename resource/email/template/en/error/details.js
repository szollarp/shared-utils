"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = function _default(notificationObject) {
  if (notificationObject.detailedErrorCode && notificationObject.detailedMessage) {
    return "<p>Details:\n        <br>Error code: ".concat(notificationObject.detailedErrorCode, "\n        <br>Error message: ").concat(notificationObject.detailedMessage, "\n        </p>");
  } else {
    return '';
  }
};

exports.default = _default;